﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HCVT.FootnoteApp.Models
{
    /// <summary>
    /// This Model is for Footnote id and Footnote Name
    /// </summary>
    public class FootnoteModel
    {
        public string footnoteid { get; set; }
        public string dealid { get; set; }
        public string vehicleid { get; set; }
        public string FootnoteName { get; set; }
    }

    /// <summary>
    /// Fund Details has three types FundLevel, Investments(Internal) and Investments(External)
    /// </summary>


    public class FundDetailsModel
    {
        public string footnoteid { get; set; }
        public string dealid { get; set; }
        public string vehicleid { get; set; }
        public string FundCode { get; set; }
        public string FundNames { get; set; }
        

    }

    /// <summary>
    /// Fund Details - Investments(Internal) 
    /// </summary>


    public class FundInternalInvestmentDetailsModel
    {
        public string FundCode { get; set; }
        public string FundInternalInvestmentNames { get; set; }
        

    }
    /// <summary>
    /// Fund Details - Investments(External)
    /// </summary>


    public class FundExternalInvestmentDetailsModel
    {
        public string FundCode { get; set; }
        public string FundExternalInvestmentNames { get; set; }

    }

    /// <summary>
    /// Investment Vehicle
    /// </summary>
    public class InvestmentVehicleModel
    {
        public string CompanyID { get; set; }
        public string InvestmentVehicleName { get; set; }
    }
    /// <summary>
    /// Deals
    /// </summary>
    public class DealsModel
    {
        public string DealID { get; set; }
        public string DealName { get; set; }
    }
    /// <summary>
    /// Section(Tabs) Details
    /// </summary>
    public class SectionDetailsModel
    {
        public string sections_id { get; set; }
        public string section_type_id { get; set; }
        public string SectionName { get; set; }
    }

    /// <summary>
    /// TextSection(Tab) Details
    /// </summary>
    public class TextDetailsModel
    {
        //public string org_id { get; set; }
        //public string session_id { get; set; }
        //public string cmpcode_id { get; set; }
        //public string footnote_id { get; set; }
        //public string header_id { get; set; }
        public string section_id { get; set; }
        public string free_text_id { get; set; }
        public string free_text { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalics { get; set; }
        public bool IsUnderline { get; set; }
    }
   /// <summary>
   /// AccountSection(Tab) Details
   /// </summary>
    public class AccountDetailsModel
    {
        public string NoOfSections { get; set; }
    }
    /// <summary>
    /// Q&ASection(Tab) Details
    /// </summary>
    public class QuestionAndAnswerDetailsModel
    {
        public string question_rpt { get; set; }
        public string question_gui_label { get; set; }
        public string answer_data_type { get; set; }
        public string description { get; set; }
        public string short_name { get; set; }
    }
    /// <summary>
    /// Save Footnote (Tab) Details
    /// </summary>
    public class SaveFootnoteDetailsModel
    {
      //public string org_id { get; set; }
      //public string session_id { get; set; }
      //public string cmpcode_id { get; set; }
      //public string footnote_id { get; set; }
      //public string header_id { get; set; }
      //public string section_id { get; set; }
      //public string sort_order { get; set; }
      //public string free_text { get; set; }
      //public string indent_number { get; set; }
      //public string is_bold { get; set; }
      //public string is_italic { get; set; }
      //public string is_underline { get; set; }

    

        public class FreeTextRowItem
        {
            public string section_id { get; set; }
            public string free_text_id { get; set; }
            public string free_text { get; set; }
            public bool IsBold { get; set; }
            public bool IsItalics { get; set; }
            public bool IsUnderline { get; set; }
            public int charCount { get; set; }
        }

        public class FreeText
        {
            public Dictionary<int,List<FreeTextRowItem>> Collection {get;set;}
        
        }

        public class QA
        {
        }

        public class Account
        {
        }

        public class RootObject
        {
            public FreeText FreeText { get; set; }
            public QA QA { get; set; }
            public Account Account { get; set; }
        }
    }
}