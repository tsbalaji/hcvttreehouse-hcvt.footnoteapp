﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class DealsController : ApiController
    {
        // GET: Deals
        [Route("api/Deals/{orgid}")]
        public IEnumerable<DealsModel> Get(string orgid)
        {
            DealsService Deals = new DealsService();
            return Deals.GetDealsDetails(orgid);
        }
    }
}