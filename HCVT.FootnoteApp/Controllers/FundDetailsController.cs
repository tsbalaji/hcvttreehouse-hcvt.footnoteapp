﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class FundDetailsController : ApiController
    {
        // GET: Fund Details
        public IEnumerable<FundDetailsModel> Get()
        {
            FundDetailsService FundDetails = new FundDetailsService();
            return FundDetails.GetFundDetails();
        }
    }
}