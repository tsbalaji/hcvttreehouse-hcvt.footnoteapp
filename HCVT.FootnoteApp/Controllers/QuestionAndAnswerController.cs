﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class QuestionAndAnswerController : ApiController
    {
        // GET: QuestionAndAnswer
        [Route("api/QuestionAndAnswer/{sectionID}")]
        public IEnumerable<QuestionAndAnswerDetailsModel> Get(string sectionId)
        {
            QuestionAndAnswerDetailsService QuestionAndAnswer = new QuestionAndAnswerDetailsService();
            return QuestionAndAnswer.GetQuestionAndAnswerDetails(sectionId);
        }
    }
}