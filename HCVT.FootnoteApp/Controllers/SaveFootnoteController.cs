﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;
using System;

namespace HCVT.FootnoteApp.Controllers
{
    
    public class SaveFootnoteController : ApiController
    {
     //   SaveFootnoteDetailsModel SaveFootnoteDetails = new SaveFootnoteDetailsModel();
        //public void post([FromBody] dynamic sectionData) {
        //public void post([FromBody] dynamic sectionData) {
        //    new SaveFootnoteService().PutFootnoteDetails()
        //    Console.WriteLine(sectionData);
        //}
      
        public string Post([FromBody] SaveFootnoteDetailsModel.RootObject SaveFootnoteDetailsValues)
        {

           string response =  new SaveFootnoteService().PutFootnoteDetails(SaveFootnoteDetailsValues);
           
           return response;
        }

    }
}