﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;


namespace HCVT.FootnoteApp.Controllers
{
    public class FootnoteController : ApiController
    {
        public IEnumerable<FootnoteModel> Get(string org_id,string session_id,string code)
        {
            FootnoteService Footnotes = new FootnoteService();
            return Footnotes.GetFootnoteDetails(org_id,session_id,code);
        }
    }
   
}
