﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class InvestmentVehicleController : ApiController
    {
        // GET: InvestmentVehicle
        [Route("api/InvestmentVehicle/{orgid}")]
        public IEnumerable<InvestmentVehicleModel> Get(string orgid)
        {
            InvestmentVehicleService InvestmentVehicle = new InvestmentVehicleService();
            return InvestmentVehicle.GetInvestmentVehicleDetails(orgid);
        }
    }
}