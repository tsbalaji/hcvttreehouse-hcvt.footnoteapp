﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class SectionController : ApiController
    {
        // GET: Section
           [Route("api/Section/{selectedFootnoteId}")]
        public IEnumerable<SectionDetailsModel> Get(string selectedFootnoteId)
        {
            SectionDetailsService SectionDetails = new SectionDetailsService();
            return SectionDetails.GetSectionDetails(selectedFootnoteId);
        }
    }
}