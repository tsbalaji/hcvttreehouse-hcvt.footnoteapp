﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class FundInternalInvestmentsController : ApiController
    {
        // GET: Fund Internal Investments
        public IEnumerable<FundInternalInvestmentDetailsModel> Get()
        {
            FundInternalInvestmentsDetailsService InternalInvestmentsDetails = new FundInternalInvestmentsDetailsService();
            return InternalInvestmentsDetails.GetFundInternalInvestmentDetails();
        }
    }
}