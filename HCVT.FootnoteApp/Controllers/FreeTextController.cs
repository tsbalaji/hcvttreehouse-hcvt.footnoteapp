﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class FreeTextController : ApiController
    {
        // GET: FreeText
         [Route("api/FreeText/{sectionID}")]
        public IEnumerable<TextDetailsModel> Get(string sectionID)
        {
            TextDetailsService FreeTextDetails = new TextDetailsService();
            return FreeTextDetails.GetTextDetails(sectionID);
        }

    }
}