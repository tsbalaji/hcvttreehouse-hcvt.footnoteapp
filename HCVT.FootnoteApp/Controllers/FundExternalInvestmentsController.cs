﻿using System.Collections.Generic;
using System.Web.Http;
using HCVT.FootnoteApp.Models;
using HCVT.FootnoteApp.Service;

namespace HCVT.FootnoteApp.Controllers
{
    public class FundExternalInvestmentsController : ApiController
    {
        // GET: Fund External Investments
        public IEnumerable<FundExternalInvestmentDetailsModel> Get()
        {
            FundExternalInvestmentsDetailsService ExternalInvestmentsDetails = new FundExternalInvestmentsDetailsService();
            return ExternalInvestmentsDetails.GetFundExternalInvestmentDetails();
        }
    }
}