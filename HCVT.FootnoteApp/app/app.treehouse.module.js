﻿(function () {
    'use strict';

    var module = angular.module('app.treehouse', [
       'treehouse.config',
        'treehouse.admin',
        'treehouse.layout',
        'treehouse.login',
        'treehouse.common',
        'treehouse.workingdata'
    ]).filter('boolText', function () {
        return function (input) {
            return input ? 'Yes' : 'No';
        }
    }).filter('range', function() {
        return function (input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i = min; i < max; i++)
                input.push(i);
            return input;
        };
    }).constant('states', {
        sessionList: 'sessions',
        sessionDetail: 'root.admin.sessionDetail'
    }).constant('auth_events', {
        loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated',
            notAuthorized: 'auth-not-authorized'
    }).run(['routeHelper', function (routeHelper) {
        routeHelper.configurePageTitle();
        routeHelper.configureRouteSecurity();
        routeHelper.configureRouteChangeError();
    }]);
})();