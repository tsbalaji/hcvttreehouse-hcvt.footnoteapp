﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [
            {
                state: 'root.userprofile',
                config: {
                    url: '/userprofile/{id}',
                    controller: 'UserProfileCtrl as vm',
                    templateUrl: resolveUrl('app/administration/userprofile/userprofile.html'),
                    resolve: {
                        model: [
                            '$stateParams', 'userProfileSvc', function ($stateParams, userProfileSvc) {
                                var userId = $stateParams.id;
                                return userProfileSvc.getUserProfile(userId);
                            }
                        ]
                    },
                    data: {
                        title: "Select Company",
                        isAuthorized: function () {
                            return true;
                        }
                    }
                }
            },
            {
                state: 'root.admin.userprofiles',
                config: {
                    url: '/userprofiles',
                    views: {
                        'main@root': {
                            controller: 'UserProfileListCtrl as vm',
                            templateUrl: resolveUrl('app/administration/userprofile/userprofilelist.html'),
                            resolve: {
                                users: [
                                    'userProfileSvc', function(userProfileSvc) {
                                        return userProfileSvc.getAllUserProfiles();
                                    }
                                ]
                            },
                            data: {
                                title: "Select Company",
                                isAuthorized: function() {
                                    return true;
                                }
                            }
                        }
                    }
                }
            },
            {
                state: 'root.admin.sessionDetail',
                config: {
                    url: '/session/{id}',
                    views: {
                        'main@root': {
                            controller: 'SessionCtrl as vm',
                            templateUrl: resolveUrl('app/administration/sessions/sessiondetail.html'),
                            resolve: {
                                organizations: [
                                    'organizationSvc', function (organizationSvc) {
                                        return organizationSvc.getOrganizationList();
                                    }
                                ],
                                model: [
                                    '$stateParams', 'sessionSvc', function ($stateParams, sessionSvc) {
                                        var sessionId = $stateParams.id;
                                        return sessionSvc.getSession(sessionId);
                                    }
                                ]
                            },
                            data: {
                                title: "Select Company",
                                isAuthorized: function () {
                                    return true;
                                }
                            }
                        }
                    }
                   
                }
            },
            {
                state: 'root.admin.session',
                config: {
                    url: '/sessions',
                    views: {
                        'main@root': {
                            controller: 'SessionListCtrl as vm',
                            templateUrl: resolveUrl('app/administration/sessions/sessionlist.html'),
                            resolve: {
                                sessions: [
                                    'sessionSvc', function (sessionSvc) {
                                        return sessionSvc.getAllSessions();
                                    }
                                ]
                            },
                            data: {
                                title: "Select Company",
                                isAuthorized: function () {
                                    return true;
                                }
                            }
                        },
                        //'content': { templateUrl: 'app/layout/tpl.main.html' },
                        //'footer@root.admin': { templateUrl: 'app/layout/tpl.footer-main.html' }
                    }
                }
            },

             {
                 state: 'adminhome',
                 config: {
                     url: '/home',
                     controller: 'AdminHomeCtrl as vm',
                     templateUrl: resolveUrl('app/administration/home/adminhome.html'),
                     data: {
                         title: "Select Company",
                         isAuthorized: function () {
                             return true;
                         }
                     }
                 }
             },


        ];
    }
})();