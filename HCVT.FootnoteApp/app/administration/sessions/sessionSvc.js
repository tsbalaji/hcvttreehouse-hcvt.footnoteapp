﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .factory('sessionSvc', sessionSvc);

    sessionSvc.$inject = ['commonSvc', 'repoBaseSvc','$state','states'];

    function sessionSvc(common, repo, $state, constants) {
        var logger = common.logger.get('sessionService');
        var sessionRepo = repo.service('Session');
        var sessionTypeRepo = repo.service('SessionType');
        var service = {
            saveSession: saveSession,
            getSession: getSession,
            getAllSessions: getAllSessions,
            cancelSessionDetailEdit: cancelSessionDetailEdit,
            canSave: canSave,
            editSession: editSession,
            deleteSession: deleteSession
           
        };

        return service;

        function saveSession(session) {
         //   $.extend(session,session.organization);
            //    $.extend(session,session.sessionType);
            var sessionId = session.sessionId;
            if (sessionId === 0) {
                return sessionRepo.create(session);
            } else {
                return sessionRepo.save(session, { 'id': sessionId });
            }
        }

        function editSession(sessionId) {
            $state.go(constants.sessionDetail, { id: sessionId });
        }

        function cancelSessionDetailEdit() {
            $state.go(constants.sessionList);
        }

        function getSession(sessionId) {
            return sessionRepo.get(sessionId);
        }

        function getAllSessions() {
            return sessionRepo.getList();
        }

        function deleteSession(sessionId) {
            return sessionRepo.remove(sessionId);
        }

        function canSave(model) {
            return !(model.sessionType != null &&
                model.organization != null &&
                (model.code != null && model.code.length > 0) &&
                 (model.description != null && model.description.length > 0));
        }
    }
})();