﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('SessionListCtrl', SessionListCtrl);

    SessionListCtrl.$inject = ['commonSvc','sessionSvc','sessions'];

    function SessionListCtrl(common, sessionSvc, sessions) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        var logger = common.logger.get('SessionCtrl');
        vm.title = 'Session List';
        vm.validationGroupId = 'vm.validation';
        vm.data = sessions;
        vm.tableId = "tblSessions";

        activate();

        function activate() {
            logger.info('Session Controller Loaded', showToaster);
        }

        vm.mappingConfig = currentMapsConfig();
        vm.mappingCommands = function (arg) {
            if (arg.commandName === 'edit') {
                sessionSvc.editSession(arg.data.sessionId);
            }
            else if (arg.commandName === 'delete') {
                common.dialogue.confirm({ message: "Are you sure you want to delete the '" + arg.data.code + "' ( '" + arg.data.description + "' ) session?" }).then(function () {
                    common.loader.show();
                    return sessionSvc.deleteSession(arg.data.sessionId).then(function () {
                        logger.debug("Session Deleted");
                    }).catch(common.errorHandler.catch(vm.validationGroupId))
                         .finally(function () {
                             return sessionSvc.getAllSessions().then(function (result) {
                                 return vm.data = result;
                             }).catch(common.errorHandler.catch(vm.validationGroupId))
                               .finally(function () {
                                   common.loader.hide();
                               });
                         });
                });

            }
        }

        function currentMapsConfig() {
            var config = {
                "columns": [],
                "columnDefs": []
            }
            config.columns.push({ "title": "Code", "data": "code" });
            config.columns.push({ "title": "Description", "data": "description" });
            config.columns.push({ "title": "Organization", "data": "organization" });
            config.columns.push({ "title": "Actions", "data": "title", "width": "5%" });
            config.columnDefs.push({
                Sortable: false,
                "render": function (data, type, row) {
                    if (data === 7) {
                        return "J";
                    }
                    return data;
                },
                "targets": -2
            });
            config.columnDefs.push({
                Sortable: false,
                "render": function (data, type, row) {
                    var control = '<a href="javascript:void(0);" data-command="edit"' +
                       ' title="Click here to edit this item." ' +
                        'class="btn-icon text-warning"><i class="fa fa-pencil fa-lg"></i></a>' +
                        '&nbsp;&nbsp;<a href="javascript:void(0);" data-command="delete"' +
                        ' title="Click here to delete this item." ' +
                        'class="btn-icon text-default"><i class="fa fa-trash-o  fa-lg"></i></a>';
                    return control;
                },
                "targets": -1
            });
            return config;
        };
    }
})();
