﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('SessionCtrl', SessionCtrl);

    SessionCtrl.$inject = ['$scope','commonSvc','sessionSvc','organizations','model'];

    function SessionCtrl($scope,common, sessionSvc, organizations, model) {
        /* jshint validthis:true */
        var vm = this;
        vm.model = {};
        var showToaster = true;
        var logger = common.logger.get('SessionCtrl');
        vm.title = 'Session';
        vm.validationGroupId = 'vm.validation';
        vm.organizations = organizations;
        vm.model = model;

        vm.onOrganizationChange = function(item) {
            vm.sessionTypes = item.sessionTypes;
        }

        $scope.$watch('vm.model.organization', function (organization) {
            if (organization)
            vm.sessionTypes = organization.sessionTypes;
        });



        vm.saveSession = function save() {
            sessionSvc.saveSession(vm.model).then(function (sessionId) {
                    sessionSvc.cancelSessionDetailEdit();
                toastr.success("Session Saved Successfully");
            })
             .catch(common.errorHandler.catch(vm.validationGroupId))
            .finally(function () {
                common.loader.hide();
            });
        }

        vm.canSave = function () {
            return sessionSvc.canSave(model);
        }

        vm.cancel = function () {
            if (!angular.equals(vm.cleanModel, vm.model))
                common.dialogue.confirm({ message: 'You have unsaved changes.  If you continue you will lose all changes.' }).then(function () {
                    sessionSvc.cancelSessionDetailEdit();
                });
            else {
                sessionSvc.cancelSessionDetailEdit();
            }
            
        }

        activate();

        function activate() {
            logger.info('Session Controller Loaded', showToaster);
            vm.model.organization = _.where(vm.organizations, { 'organizationId': 2 }, 'organization');
            setSessionAttributes();
            vm.cleanModel = angular.copy(vm.model);
        }

        function setSessionAttributes() {
            var organization;
            var sessionType;
            var organizationId = undefined;
            var sessionTypeId = undefined;
            if (vm.model.organization)
                organizationId = vm.model.organization[0].organizationId;
            if (vm.model.sessionType)
                sessionTypeId = vm.model.sessionType.sessionTypeId;

            if (sessionTypeId && organizationId) {
                organization = _.where(vm.organizations, { 'organizationId': organizationId });
                sessionType = _.where(organization[0].sessionTypes, { 'sessionTypeId': sessionTypeId });

                vm.model.organization = organization[0];
                vm.model.sessionType = sessionType[0];
            }
        }
    }
})();
