﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .factory('organizationSvc', organizationSvc);

    organizationSvc.$inject = ['commonSvc', 'repoBaseSvc'];

    function organizationSvc(common, repo) {
        var logger = common.logger.get('organizationService');
        var organizationRepo = repo.service('Organization');
       
        var service = {
            getOrganizationList: getOrganizationList
           
        };

        return service;

        function getOrganizationList() {
            return organizationRepo.getList({});
        }

        
    }
})();