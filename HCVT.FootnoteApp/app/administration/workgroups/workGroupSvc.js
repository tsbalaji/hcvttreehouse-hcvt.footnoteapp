﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .factory('workGroupSvc', workGroupSvc);

    workGroupSvc.$inject = ['commonSvc', 'repoBaseSvc'];

    function workGroupSvc(common, repo) {
        var logger = common.logger.get('workGroupService');
        var workGroupRepo = repo.service('WorkGroup');
       
        var service = {
            saveWorkGroup: saveWorkGroup,
            getWorkGroup: getWorkGroup
           
        };

        return service;

        function saveWorkGroup(workGroup) {
            return workGroupRepo.create(workGroup);
        }

        function getWorkGroup(workGroupId) {
            return workGroupRepo.get(workGroupId);
        }

        
    }
})();