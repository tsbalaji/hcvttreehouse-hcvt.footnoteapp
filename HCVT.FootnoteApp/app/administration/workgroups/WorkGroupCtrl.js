﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('WorkGroupCtrl', WorkGroupCtrl);

    WorkGroupCtrl.$inject = ['commonSvc','workGroupSvc','organizations'];

    function WorkGroupCtrl(common, workGroupSvc, data) {
        /* jshint validthis:true */
        var vm = this;
        vm.model = {};
        var showToaster = true;
        var logger = common.logger.get('WorkGroupCtrl');
        vm.title = 'Work Group';
        vm.validationGroupId = 'vm.validation';
        vm.organizations = data.organizations;
        vm.model = data.model;
       
        vm.saveSession = function save() {
            vm.model.state = vm.model.workGroupId === -1 ? 1 : 3; 
            workGroupSvc.saveWorkGroup(vm.model).then(function() {
                toastr.success("Session Saved Successfully");
            })
             .catch(common.errorHandler.catch(vm.validationGroupId))
            .finally(function () {
                common.loader.hide();
            });
        }

        activate();

        function activate() {
            logger.info('WorkGroup Controller Loaded', showToaster);
        }

        

        
    }
})();
