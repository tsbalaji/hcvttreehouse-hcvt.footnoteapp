﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .factory('userProfileSvc', userProfileSvc);

    userProfileSvc.$inject = ['commonSvc', 'repoBaseSvc','$state'];

    function userProfileSvc(common, repo, $state) {
        var logger = common.logger.get('userProfileService');
        var roleRepo = repo.service('/Admin/UserProfile/Role');
        var organizationRepo = repo.service('/Admin/UserProfile/Organization');
        var userAccessRepo = repo.service('/Admin/UserProfile/UserAccess');
        var userProfileRepo = repo.service('UserProfile');
       
        var service = {
            getRoles: getRoles,
            getOrganizations: getOrganizations,
            getUserAccessModel: getUserAccessModel,
            getUserProfile: getUserProfile,
            canSaveUserProfile: canSaveUserProfile,
            saveUserProfile: saveUserProfile,
            getAllUserProfiles: getAllUserProfiles,
            editUser: editUser,
            cancelUserProfileEdit: cancelUserProfileEdit,
            deleteUserProfile: deleteUserProfile
        };

        return service;

        function getRoles() {
            return roleRepo.getList();
        }

        function getOrganizations() {
            return organizationRepo.getList();
        }

        function getUserAccessModel(user, organizationId) {
            return userAccessRepo.get(organizationId, {'userId': user});
        }

        function getUserProfile(userId) {
            return userProfileRepo.get(userId);
        }

        function getAllUserProfiles() {
            return userProfileRepo.getList();
        }

        function canSaveUserProfile(model) {
            var canSave = !((model.email != null && model.email.length > 0) &&
            (model.firstName != null && model.firstName.length > 0) &&
            (model.lastName != null && model.lastName.length > 0) &&
            (model.employer != null && model.employer.length > 0) &&
            ((model.userId !== '' && model.userId != undefined) || (model.password != null && model.password.length >= 6)));
               

            return canSave;
        }

        function editUser(userId) {
            $state.go('userprofile', { id: userId });
        }

        function cancelUserProfileEdit() {
            $state.go('manageusers');
        }

        function saveUserProfile(userProfile) {
            if (userProfile.userId)
                return userProfileRepo.save(userProfile, { 'id': userProfile.userId });
            else {
                return userProfileRepo.create(userProfile);
            }
           
        }

        function deleteUserProfile(userProfileId) {
            return userProfileRepo.remove(userProfileId);
        }

        
    }
})();