﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('UserProfileCtrl', UserProfileCtrl);

    UserProfileCtrl.$inject = ['$scope','commonSvc','userProfileSvc','$modal','model','$state'];

    function UserProfileCtrl($scope,common,userProfileSvc,$modal,model,$state) {
        /* jshint validthis:true */
        var vm = this;
        vm.model = model;
      
        vm.accountState = [
            {
             'id': 1,
             'text':'Active'   
            },
            {
              'id': 2,
              'text': 'Inactive'
            },
            {
              'id': 3,
              'text': 'Locked'
            }
        ];
        var showToaster = true;
        var logger = common.logger.get('UserProfileCtrl');
        vm.title = 'User Profile';
        vm.validationGroupId = 'vm.validation';
        vm.openModal = showAddOrganizationModal;
        vm.canSave = function() {
            return userProfileSvc.canSaveUserProfile(vm.model);
        }

        vm.save = function() {
            return userProfileSvc.saveUserProfile(vm.model).then(function () {
                    $state.go('manageusers');
                toastr.success("User Created Successfully");
            })
             .catch(common.errorHandler.catch(vm.validationGroupId))
            .finally(function () {
                common.loader.hide();
            });
        }

        vm.cancel = function () {
           // if (!angular.equals(vm.cleanModel, vm.model))
              //  common.dialogue.confirm({ message: 'You have unsaved changes.  If you continue you will lose all changes.' }).then(function () {
                    userProfileSvc.cancelUserProfileEdit();
               // });
            //else {
            //    sessionSvc.cancelSessionDetailEdit();
            //}

        }

        activate();

        $scope.$watch('vm.model.email', function(value) {
            vm.model.userName = value;
        });
        vm.onOrganizationChange = function (item) {
            userProfileSvc.getUserAccessModel(item.organizationId).then(function(result) {
                vm.model = result;
            });
        }
       
        function activate() {
            //common.loader.show();
            logger.info('User Profile Loaded', showToaster);
            vm.model.status = _.where(vm.accountState, { 'id': model.status.id })[0];
            vm.cleanModel = angular.copy(vm.model);
        }

        function showAddOrganizationModal() {

            var modalInstance = $modal.open({
                templateUrl: 'app/administration/userprofile/useraccess.html',
                controller: 'UserAccessCtrl',
                controllerAs: 'vm',
                backdrop: 'static',
                resolve: {
                    //workGroups: ['repoBaseSvc', function (repo) {
                    //    var wgRepo = repo.service('Admin/UserProfile/WorkGroup');
                    //    return wgRepo.getList();
                    //}],
                    organizations: [
                        'repoBaseSvc', function(repo) {
                            var wgRepo = repo.service('Admin/UserProfile/Organization');
                            return wgRepo.getList();
                        }
                    ],
                    roles: [
                        'repoBaseSvc', function(repo) {
                            var wgRepo = repo.service('Admin/UserProfile/Role');
                            return wgRepo.getList();
                        }
                    ],
                    userId: function() {
                        return vm.model.userId;
                    }
        }
               
            });

            modalInstance.result.then(function(result) {
                vm.model.userAccess = result;
            });
        }

    }
})();


