﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('UserAccessCtrl', UserAccessCtrl);
    UserAccessCtrl.$inject = ['$scope','$modalInstance','commonSvc','userProfileSvc','organizations', 'roles','userId'];

    function UserAccessCtrl($scope, modal,common,userProfileSvc,organizations, roles, userId) {
        /* jshint validthis:true */
        var vm = this;
        vm.model = {};
        var showToaster = true;
        var logger = common.logger.get('UserAccessCtrl');
        vm.title = 'Select Company';
        vm.validationGroupId = 'vm.validation';
        vm.roles = roles;
        vm.organizations = organizations;
        var user = userId;
        vm.cancel = cancel;
        vm.save = save;
        vm.allWorkGroups = 'false';
        vm.allSessions = 'false';
        vm.onOrganizationChange = function(item) {
            userProfileSvc.getUserAccessModel(user, item.organizationId).then(function(result) {
                vm.model = result;
                setAllWorkGroups();
                setRole();
            });
        }
        activate();

        $scope.$watch('vm.selectedRole', function(role) {
            if (vm.allWorkGroups === 'true') {
                $.each(vm.model.accessList, function(index, workGroup) {
                    workGroup.role = {roleId: role.roleId, roleName: role.roleName};
                });
            }
        });

        function activate() {
            logger.info('Context Control Loaded', showToaster);
        }

        function setAllWorkGroups() {
            var allWorkGroups = true;
            $.each(vm.model.accessList, function(index, item) {
                allWorkGroups = allWorkGroups && item.isSelected;
            });

            vm.allWorkGroups = allWorkGroups.toString();
        }

        function cancel() {
            modal.dismiss('cancel');
        }

        function save() {
            modal.close(vm.model);
        }

        function setRole() {
            if (vm.allWorkGroups === 'true') {
                if (vm.model.accessList[0].role) {
                    var roleId = vm.model.accessList[0].role.roleId;
                    vm.selectedRole = _.where(vm.roles, { 'roleId': roleId })[0];
                }
            }
        }

    }
})();
