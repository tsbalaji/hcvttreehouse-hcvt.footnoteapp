﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('UserProfileListCtrl', UserProfileListCtrl);

    UserProfileListCtrl.$inject = ['commonSvc','userProfileSvc','users'];

    function UserProfileListCtrl(common, userProfileSvc, users) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        var logger = common.logger.get('UserProfileCtrl');
        vm.title = 'User Profiles';
        vm.validationGroupId = 'vm.validation';
        vm.data = users;
        vm.tableId = "tblUsers";

        activate();

        function activate() {
            logger.info('User Profile Controller Loaded', showToaster);
        }

        vm.mappingConfig = currentMapsConfig();
        vm.mappingCommands = function (arg) {
            if (arg.commandName === 'edit') {
                userProfileSvc.editUser(arg.data.userId);
            }
            else if (arg.commandName === 'delete') {
                common.dialogue.confirm({ message: "Are you sure you want to delete '" + arg.data.userName + "'?" }).then(function () {
                    common.loader.show();
                    return userProfileSvc.deleteUserProfile(arg.data.userId).then(function () {
                        logger.debug("User Deleted");
                    }).catch(common.errorHandler.catch(vm.validationGroupId))
                         .finally(function () {
                             return userProfileSvc.getAllUserProfiles().then(function (result) {
                                 return vm.data = result;
                             }).catch(common.errorHandler.catch(vm.validationGroupId))
                               .finally(function () {
                                   common.loader.hide();
                               });
                         });
                });

            }
        }

        function currentMapsConfig() {
            var config = {
                "columns": [],
                "columnDefs": []
            }
            config.columns.push({ "title": "Username", "data": "userName" });
            config.columns.push({ "title": "First Name", "data": "firstName" });
            config.columns.push({ "title": "Last Name", "data": "lastName" });
            config.columns.push({ "title": "Email", "data": "email" });
            config.columns.push({ "title": "Employer", "data": "employer" });
           // config.columns.push({ "title": "Staus" });
            config.columns.push({ "title": "Actions", "data": "title", "width": "5%" });
            config.columnDefs.push({
                Sortable: false,
                "render": function (data, type, row) {
                    if (data === 7) {
                        return "J";
                    }
                    return data;
                },
                "targets": -2
            });
            config.columnDefs.push({
                Sortable: false,
                "render": function (data, type, row) {
                    var control = '<a href="javascript:void(0);" data-command="edit"' +
                       ' title="Click here to edit this item." ' +
                        'class="btn-icon text-warning"><i class="fa fa-pencil fa-lg"></i></a>' +
                        '&nbsp;&nbsp;<a href="javascript:void(0);" data-command="delete"' +
                        ' title="Click here to delete this item." ' +
                        'class="btn-icon text-default"><i class="fa fa-trash-o  fa-lg"></i></a>';
                    return control;
                },
                "targets": -1
            });
            return config;
        };
    }
})();
