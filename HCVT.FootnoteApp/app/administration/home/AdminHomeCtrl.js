﻿(function () {
    'use strict';

    angular
        .module('treehouse.admin')
        .controller('AdminHomeCtrl', AdminHomeCtrl);

    AdminHomeCtrl.$inject = ['commonSvc', 'repoBaseSvc'];

    function AdminHomeCtrl(common,repo) {
        /* jshint validthis:true */
        var vm = this;
       

        activate();

        function activate() {
            vm.menu = {};
        }
    }
})();
