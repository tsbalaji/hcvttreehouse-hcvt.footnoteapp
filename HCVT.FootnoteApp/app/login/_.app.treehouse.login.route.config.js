﻿(function () {
    'use strict';

    angular
        .module('treehouse.login')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [
            {
                state: 'root.login',
                config: {
                    url: '^/login',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/login/login.html'),
                            controller: 'LoginCtrl as vm'
                        },
                        'header@root' : {
                            templateUrl: resolveUrl('app/login/tpl.header-login.html')
                        }
                        //'content': { templateUrl: 'app/layout/tpl.main.html' },
                        //'footer@root.admin': { templateUrl: 'app/layout/tpl.footer-main.html' }
                    },
                    data: {
                        hideNavigation: true
                    }
                }
            }
        ];
    }
})();