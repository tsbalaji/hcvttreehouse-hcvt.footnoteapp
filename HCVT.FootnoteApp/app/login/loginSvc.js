(function () {
    'use strict';

    angular
        .module('treehouse.login')
        .factory('loginSvc', loginSvc);

    loginSvc.$inject = ['$sessionStorage','commonSvc','authSvc','$state'];

    function loginSvc($sessionStorage,common, authSvc, $state) {


        var service = {
            login: login,
            isAuthenticated : isAuthenticated
        }

        function isAuthenticated(){
            if (authSvc.isAuthenticated())
                $state.go('root.selectcompany');
        }

        function login (name, password) {
           return authSvc.login(name,password)
        };



        return service;
    }
})();