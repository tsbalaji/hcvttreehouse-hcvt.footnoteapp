﻿(function () {
    'use strict';

    angular
        .module('treehouse.login')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope','commonSvc','$rootScope','loginSvc', '$state'];

    function LoginCtrl($scope,common,$rootScope,loginSvc,$state) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        var logger = common.logger.get('LoginCtrl');
        vm.title = 'Login';
        //vm.menu = [];

        $rootScope.htmlClass = 'login';

        activate();

        function activate() {
            logger.info('Login Controller Loaded', showToaster);
            //loginSvc.isAuthenticated();
        }

        vm.login = function() {

            vm.errorMessage = false;
            $('.login-button').button('loading');

            loginSvc.login(vm.userName, vm.password)
                .then(function(result){
                    $state.go('root.selectcompany');
                },
                /* error */
                function(result){
                    $('.login-button').button('reset');
                    vm.errorMessage = result.data.error_description;
                });
        }

       
    }
})();
