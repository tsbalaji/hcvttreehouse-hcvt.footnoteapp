﻿(function () {
    'use strict';

    angular
        .module('treehouse.login')
        .factory('authSvc', authSvc);

    authSvc.$inject = ['$rootScope','$sessionStorage','repoBaseSvc', '$state','jwtHelper','auth_events'];

    function authSvc($rootScope,$sessionStorage, repo, $state,jwtHelper,auth_events) {


        var service = {
            login: login,
            logout: logout,
            isAuthenticated: function () {
                var authData = $sessionStorage.authData;
               if (authData){
                   return true;
               }

                return false;
            },
            username: userFullName,
            role: function () { return role; },
            fullName: function(){return fullName; }
        }
        var fullName = '';
        var loginRepo = repo.service('login');
        var authentication = {
            isAuth: false,
            userName : ""
        };

        $rootScope.$on(auth_events.notAuthenticated,function(){
            logout();
        });
        var register = function (registration) {

            logout();

            return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
                return response;
            });

        };

        function login (name, password) {
            return loginRepo.saveForm({ "username":name, "password":password, "grant_type":"password" }).then(function(result) {
              fullName = jwtHelper.decodeToken(result.access_token).fullname;

              return  $sessionStorage.authData = {token: result.access_token, userName:name, fullName:fullName}
            });
        };

        function logout() {

            delete $sessionStorage.authData;

            authentication.isAuth = false;
            authentication.userName = "";
            $state.go('root.login');

        };

        function userFullName() {

            var authData = $sessionStorage.authData;
            if (authData)
            {
                return authData.fullName
            }

            return '';
        }


        return service;
    }
})();