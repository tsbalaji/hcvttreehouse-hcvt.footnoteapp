//Extensions for Underscore
(function (_) {
    _.mixin({
        //Returns true if argument is null or undefined
        isNullOrUndefined: function (arg) {
            return _.isNull(arg) || _.isUndefined(arg);
        },
        endsWith: function (string, pattern) {
            var d = string.length - pattern.length;
            return d >= 0 && string.indexOf(pattern, d) === d;
        },
        hasValue: function (arg) {
            var isValid = !_.isUndefined(arg) && !_.isNull(arg);
            if (isValid) {
                isValid = _.size(arg) > 0;
            }
            return isValid;
        }
    });
})(window._);