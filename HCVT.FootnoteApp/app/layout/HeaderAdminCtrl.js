﻿(function () {
    'use strict';

    angular
        .module('treehouse.layout')
        .controller('HeaderAdminCtrl', HeaderAdminCtrl);

    HeaderAdminCtrl.$inject = ['$scope','commonSvc','repoBaseSvc','authSvc'];

    function HeaderAdminCtrl($scope,common,repoBase,authSvc) {
        /* jshint validthis:true */
        var vm = this;
        var navRepo = repoBase.service('navigation');
        var showToaster = true;
        var logger = common.logger.get('HeaderAdminCtrl');
        vm.title = 'Layout';
        vm.userName = authSvc.fullName();

        activate();

        function activate() {
            logger.info('Header Controller Loaded', showToaster);
            navRepo.getList().then(function (result) {
                vm.menu = result;
                $scope.$broadcast('navigationLoaded');
            });
        }

       
    }
})();
