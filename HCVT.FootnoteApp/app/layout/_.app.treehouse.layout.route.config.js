﻿(function() {
    'use strict';

    angular
        .module('treehouse.layout')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [
            {
                state: 'root',
                config: {
                    url: '/',
                    views: {
                        '@': {
                            templateUrl: 'app/layout/layout.html',
                            controller: 'IndexCtrl'
                        },
                        'header@root': {
                            templateUrl: 'app/layout/tpl.header-main.html',
                            controller: 'HeaderMainCtrl as vm'
                        },
                        'content': { templateUrl: 'app/layout/tpl.main.html' },
                        'footer@root': { templateUrl: 'app/layout/tpl.footer-main.html' }
                    }
                }

            },
             {
                 state: 'root.admin',
                 config: {
                     url: '^/admin',
                     views: {
                         'header@root': {
                             templateUrl: 'app/layout/tpl.header-admin.html',
                             controller: 'HeaderAdminCtrl as vm'
                         }
                         //'content': { templateUrl: 'app/layout/tpl.main.html' },
                         //'footer@root.admin': { templateUrl: 'app/layout/tpl.footer-main.html' }
                     }
                 }
             }
        ];
    }

    
})();