﻿(function () {
    'use strict';

    angular
        .module('treehouse.layout')
        .controller('IndexCtrl', IndexCtrl);

    IndexCtrl.$inject = ['$scope','commonSvc','repoBaseSvc'];

    function IndexCtrl($scope,common,repoBase) {
        /* jshint validthis:true */
        var vm = this;
        var sessionRepo = repoBase.service('Session');
        var showToaster = true;
        var logger = common.logger.get('IndexCtrl');
        vm.title = 'Layout';
       
        activate();

        function activate() {
            logger.info('Session Controller Loaded', showToaster);
          
        }

       
    }
})();
