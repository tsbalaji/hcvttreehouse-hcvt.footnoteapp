﻿(function () {
    'use strict';

    angular
        .module('treehouse.layout')
        .controller('HeaderMainCtrl', HeaderMainCtrl);

    HeaderMainCtrl.$inject = ['$scope','commonSvc','repoBaseSvc','authSvc','contextSvc', '$sessionStorage'];

    function HeaderMainCtrl($scope,common,repoBase,authSvc,contextSvc,$sessionStorage) {
        /* jshint validthis:true */
        var vm = this;
        var navRepo = repoBase.service('navigation');
        var showToaster = true;
        var logger = common.logger.get('HeaderMainCtrl');

        vm.title = 'Layout';
        vm.$storage = $sessionStorage;
        vm.logout = function(){
            authSvc.logout();
        }

        activate();

        function activate() {
            logger.info('Header Main Controller Loaded', showToaster);
            navRepo.getList().then(function (result) {
                vm.menu = result;
                updateContext();
                vm.menuHelper = menuHelper.process(result);
                $scope.$broadcast('navigationLoaded');
            });
        }

        function updateContext(){

            var context = contextSvc.getCurrentContext();

            vm.context = {
                session: context.session.code,
                company: context.company.code + '(' + context.company.code + ')'
            };

        }

        $scope.$watch(authSvc.fullName,function(){
            vm.userName = authSvc.fullName();
        })

        $(document).on('click', 'a[data-nav-redirect]', function(e){
            e.preventDefault();

            var context = contextSvc.getCurrentContext();

            var url = $(this).attr('href')
                + '&p_org=' + context.organization.organizationId + '&'
                + 'p_company=' + context.company.companyId + '&'
                + 'p_session=' + context.session.sessionId;

            window.open(url);
        })

        var menuHelper = {
            process: function(menu){
                var menuData = [];
                $.each(menu, function(index, child){

                    var colCount = menuHelper.columns(child);

                    menuData.push({
                        'key': index,
                        'cols': colCount,
                        'colClass': menuHelper.colClass(colCount),
                        'class': 'columns-' + colCount

                    })
                })
                return menuData;
            },
            columns: function(menu){
                var columnCount = 0;
                $.each(menu.children, function(index, key){
                    if(key.isHeader && key.isVisible)
                        columnCount++;

                })
                return columnCount;
            },
            colClass: function(count){
                switch(count){
                    case 0:
                        return '';
                    case 1:
                        return 'col-sm-12';
                        break;
                    case 2:
                        return 'col-sm-6';
                        break;
                    case 3:
                        return 'col-sm-4';
                        break;
                    case 4:
                        return 'col-sm-3';
                        break;
                    default:
                        return 'col-sm-12'
                }
            }
        }
    }
})();
