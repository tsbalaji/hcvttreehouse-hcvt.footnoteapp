﻿(function () {
    'use strict';

    angular
        .module('treehouse.common')
        .controller('FooterCtrl', FooterCtrl);

    FooterCtrl.$inject = ['$sessionStorage','commonSvc','contextSvc'];

    function FooterCtrl($sessionStorage,common,contextSvc) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        var logger = common.logger.get('ContextCtrl');
        vm.$storage = $sessionStorage;
        vm.context = contextSvc.getCurrentContext;
        activate();

        function activate() {
            //common.loader.show();
            logger.info('Context Control Loaded', showToaster);
        }

    }
})();
