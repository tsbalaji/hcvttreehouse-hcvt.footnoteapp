﻿(function () {
    'use strict';

    angular
        .module('treehouse.common')
        .factory('contextSvc', contextSvc);

    contextSvc.$inject = ['commonSvc', 'repoBaseSvc', '$state', 'states', '$sessionStorage'];

    function contextSvc(common, repo, $state, constants, $sessionStorage) {
        var logger = common.logger.get('contextService');
        var orgRepo = repo.service('UserContext');
        var sessionRepo = repo.service('api/Organization/User/OrganizationHistory');
      var sessionContext = {
          'organization': '',
          'session': '',
          'workgroup': '',
          'company': ''
      }
        var company;
        var service = {
            getUserOrganizations: getUserOrganizations,
            getCurrentContext: getCurrentContext,
            setUserContext: setUserContext,
            canSave: canSave,
            getPreviousSession: getPreviousSession,
            saveWorkingSession: saveWorkingSession,
            goWorkingDataHome: goWorkingDataHome
        };

        return service;
        function getUserOrganizations() {
            return orgRepo.getList();
        }

        function getPreviousSession() {
            return sessionRepo.get();
    }


        function saveWorkingSession(context) {
            return sessionRepo.create(context);
        }

        function getCurrentContext() {
            return $sessionStorage.userContext;
        }

        function setUserContext() {
            goWorkingDataHome()

        }

        function goWorkingDataHome() {
            $state.go('root.workingdatahome');
        }
        function canSave(model) {
            return !model || !model.selectedOrganization || !model.selectedSession || !model.selectedCompany;
        }
    }
})();