﻿(function () {
    'use strict';

    angular
        .module('treehouse.common')
        .controller('ContextCtrl', ContextCtrl);

    ContextCtrl.$inject = ['$sessionStorage','commonSvc','contextSvc','organizations'];

    function ContextCtrl($sessionStorage,common,contextSvc,organizations) {
        /* jshint validthis:true */
        var vm = this;
        vm.model = {};
        var showToaster = true;
        var logger = common.logger.get('ContextCtrl');
        vm.$storage = $sessionStorage;
        vm.title = 'Select Company';
        vm.validationGroupId = 'vm.validation';
        vm.organizations = organizations;
       // vm.companies = data.companies;
        //vm.sessions = vm.selected.sessions;
        //vm.workgroups = organizations.workgroups;
        activate();
        vm.canSave = function() {
            return contextSvc.canSave(vm.model);
        }
      
        vm.onOrganizationChange = function (org) {
            if (org) {
                resetSelections(org);
            }
        }

        vm.onSessionChange = function (session) {

        }

        vm.onWorkGroupChange = function (workgroup) {
            if (workgroup.code) {
                vm.companies = workgroup.companies;
                vm.model.selectedCompany = undefined;
            } else {
                vm.companies = vm.model.selectedOrganization.companies;
                vm.model.selectedCompany = undefined;
            }
        }

        vm.onCompanyChange = function(company) {
            
        }

        vm.save = function () {
            vm.$storage.userContext = { 'organization': vm.model.selectedOrganization, 'session': vm.model.selectedSession, 'workgroup': vm.model.selectedWorkgroup, 'company': vm.model.selectedCompany };
            contextSvc.saveWorkingSession(vm.$storage.userContext);
            contextSvc.setUserContext();

        }

        vm.cancel = function() {
            contextSvc.goWorkingDataHome();
        }

        function resetSelections(org) {
            vm.sessions = org.sessions;
            org.workGroups.push({ code: undefined });
            vm.workgroups = org.workGroups;
            vm.companies = org.companies;
            vm.model.selectedCompany = undefined;
            vm.model.selectedWorkgroup = undefined;
            vm.model.selectedSession = undefined;
        }

        //$scope.$watch('vm.selectedOrganization', function(organization) {
        //    vm.sessions = organization ? organization.sessions : {};
        //});
       
        function activate() {
            //common.loader.show();
             contextSvc.getPreviousSession().then(function(context) {
                 if (context != undefined) {
                     var userContext = JSON.parse(context);
                     //vm.$storage.userContext = userContext;
                     setContextSelections(userContext);
                 }
             });
           
            logger.info('Context Control Loaded', showToaster);
            //vm.selectedWorkgroup = {code:undefined}
        }

        function setContextSelections(context) {
            var org = _.where(vm.organizations, { 'organizationId': context.organization.organizationId })[0];
            vm.model.selectedOrganization = org;
            resetSelections(org);
            vm.model.selectedSession = _.where(vm.sessions, { 'sessionId': context.session.sessionId })[0];
            if (context.workgroup)
                vm.model.selectedWorkGroup = _.where(vm.workggroups, { 'workGroupid': context.workgroup.workGroupId })[0];
            vm.model.selectedCompany = _.where(vm.companies, { 'companyId': context.company.companyId })[0];
        }



    }
})();
