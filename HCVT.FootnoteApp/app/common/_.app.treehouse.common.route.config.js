﻿(function () {
    'use strict';

    angular
        .module('treehouse.common')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [

            {
                state: 'root.selectcompany',
                config: {
                    url: '^/selectcompany',
                    views:{
                        'main@root':{
                            controller: 'ContextCtrl as vm',
                            templateUrl: resolveUrl('app/common/usercontext/context.html'),
                            resolve: {
                                organizations: ['contextSvc', function (contextSvc) {
                                    return contextSvc.getUserOrganizations();
                                }]
                            },
                            data: {
                                title: "Select Company",
                                isAuthorized: function () {
                                    return true;
                                }
                            }
                        }
                    },
                    data: {
                        hideNavigation: true
                    }
                }
            }

        ];
    }
})();