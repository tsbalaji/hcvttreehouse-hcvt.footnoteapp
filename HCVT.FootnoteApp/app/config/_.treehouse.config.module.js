﻿(function () {
	'use strict';

	angular.module('treehouse.config', [
     // Angular modules 
        'ngAnimate', 'ngSanitize',
        // Custom modules 
        'bmall.core',
        // 3rd Party Modules
        'ui.router',
        'ngFileUpload',
        'ngStorage',
        'angular-jwt',
        'localytics.directives',
        'mgcrea.ngStrap',
        'ui.checkbox'
	]);
})();