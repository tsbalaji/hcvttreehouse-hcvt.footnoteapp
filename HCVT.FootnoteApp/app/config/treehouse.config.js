﻿(function () {
    'use strict';

    var core = angular.module('treehouse.config');

    core.config(configure);

    configure.$inject = [
        '$logProvider', '$stateProvider', 'routeHelperConfigProvider',
        '$urlRouterProvider', 'validationHelperConfigProvider', 'repoBaseConfigProvider'
    ];

    function configure(
        $logProvider, $stateProvider, routeHelperConfigProvider,
        $urlRouterProvider, validationHelperConfigProvider, repoBaseConfigProvider
        ) {

        configureLogging();
        configureRouting();
        configureValidations();
        configureBaseRepository();

        function configureLogging() {
            // turn debugging off/on (no info or warn)
            if ($logProvider.debugEnabled) {
                $logProvider.debugEnabled(false);
            }
        }

        function configureRouting() {
            var routeCfg = routeHelperConfigProvider;
            routeCfg.config.$stateProvider = $stateProvider;
            routeCfg.config.$urlRouterProvider = $urlRouterProvider;
            routeCfg.config.defaultRoute = '/login';
            routeCfg.config.docTitle = 'TreeHouse-';
            routeCfg.config.errorState = 'oops';
        }

        function configureValidations() {
            //$templateRequest('')
            //               .then(function processTemplate(html) {
            //                   validationHelperConfigProvider.config.setValidationConfig(html);
            //               });

            var html =
                '<span ng-message=\"required\">#FRIENDLYNAME# field is required</span>' +
                '<span ng-message=\"email\">#FRIENDLYNAME# is invalid</span>' +
                '<span ng-message=\"minlength\">#FRIENDLYNAME#  is Too small</span>' +
                '<span ng-message=\"max\">#FRIENDLYNAME too large</span>' +
                '<span ng-message=\"treehousePattern\">#FRIENDLYNAME#  Invalid format</span>' +
                '<span ng-message=\"treehouseSelectedDaysRequired\">#FRIENDLYNAME#  Please select at least one day.</span>' +
                '<span ng-message=\"treehouseDefaultValueValid\">Default value not valid based on the format of the control type.</span>' +
                '<span ng-message=\"startsWith\">Invalid starting character</span>' +
                '<span ng-message=\"username\">Invalid username</span>';
            validationHelperConfigProvider.config.setValidationConfig(html);
        }

        function configureBaseRepository() {
            repoBaseConfigProvider.config = {
                baseUrl: 'http://172.24.17.160/TreeHouseDevApi/'
            };
        }
    }
})();