﻿(function () {
    'use strict';

    var core = angular
        .module('treehouse.config');

    var constants = {
        routes: {
            sessionList: 'sessionlist'
        },
        auth_events: {
        loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated',
            notAuthorized: 'auth-not-authorized'
        }
    };

    core.value('constants', constants);
})();
