﻿(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .controller('WorkingDataHomeCtrl', WorkingDataHomeCtrl);

    WorkingDataHomeCtrl.$inject = [];

    function WorkingDataHomeCtrl() {
        /* jshint validthis:true */

        activate();

        function activate() {

        }
    }
})();
