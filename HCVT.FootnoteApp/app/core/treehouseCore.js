/*Version 1.0.36*/
(function () {
    'use strict';

    angular.module('treehouse.core', [
        'treehouse.diagnostics', 'treehouse.router', 'treehouse.data', 'treehouse.modal',
        'treehouse.form', 'treehouse.security', 'treehouse.ui', 'treehouse.util'
    ]).config(configure);
    configure.$inject = ['$logProvider'];

    function configure($logProvider) {
        //if (treehouse.client.configuration.settings.isDebugMode) {
        //    if ($logProvider.debugEnabled) {
        //        $logProvider.debugEnabled(true);
        //    }
        //}
    }
})();
(function (window, $, undefined) {
    'use strict';
    var treehouse = window.treehouse || {};
    treehouse.client = {};
    treehouse.client.util = (function () {
        var contract = {
            resolveUrl: resolveUrl,
            extend: extend,
            getRandomKey: getRandomKey,
            getEmptyGuid: getEmptyGuid,
            getQueryStringParameterByName: getQueryStringParameterByName
        };

        function resolveUrl(url) {
            if (url != null && url.indexOf !== undefined) {
                if (url.indexOf("http") === 0) {
                    return url;
                }
                var index = url.indexOf('~');

                if (index >= 0) {
                    url = url.substr(index + 1);
                }
                if (treehouse.client.configuration.settings !== undefined &&
                    treehouse.client.configuration.settings.virtualDirectory !== "/") {
                    url = treehouse.client.configuration.settings.virtualDirectory + url;
                }

                if (endsWith(url, ".html")) {
                    //If calling a html template we need to lowercase the url settings
                    //take advantage of angular template cache
                    return url.toLowerCase();
                }
                return url;
            }
            return '';
        }

        function extend() {
            return $.extend(arguments[0], arguments[1]);
        }
        function getRandomKey() {
            return Math.random() * 100000000000000000;
        }

        function getEmptyGuid() {
            function n() {
                return Math.floor(65536).toString(16).substring(1);
            }
            return n() + n() + "-" + n() + "-" + n() + "-" + n() + "-" + n() + n() + n();
        }

        function getQueryStringParameterByName(param) {
            var pageUrl = window.location.hash.substring(1);
            var urlVariables = pageUrl.split('?');
            for (var i = 0; i < urlVariables.length; i++) {
                var sParameterName = urlVariables[i].split('=');
                if (sParameterName[0] == param) {
                    return sParameterName[1];
                }
            }
        }

        function endsWith(string, pattern) {
            var d = string.length - pattern.length;
            return d >= 0 && string.indexOf(pattern, d) === d;
        }

        return contract;

    }());


    treehouse.client.configuration = (function () {
        var contract = {
            init: init,
            settings: settings
        },
            defaultConfiguration = {
                isDebugMode: false
            },
            settings = {};

        function init(config) {
            treehouse.client.configuration.settings = $.extend({}, defaultConfiguration, config);
        }

        return contract;
    }());

    //treehouse.client.user = (function() {
    //    var contract = {
    //            init: init,
    //            current: user
    //        },
    //        defaultUser = {
    //            serverEndpoint: null
    //        },
    //        user = {};

    //    function init(currentUser) {
    //        user = treehouse.client.util.extend(defaultUser, currentUser);
    //    }
    //    return contract;
    //}());
    //Initialize this while bootstrapping application
    //treehouse.client.app = {};
    window.treehouse = treehouse;

}(window, jQuery));
(function () {
    'use strict';

    angular.module('third.party', [])
        .factory('toastr', function () {
            return window.toastr;
        }).factory('underscore', function () {
            return window._;
        });
})();
(function () {
    'use strict';

    angular.module('treehouse.data', ['treehouse.diagnostics']);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.data')
        .factory('applicationConfigSvc', applicationConfigSvc);


    function applicationConfigSvc() {
        var service = {
            settings: getSettings()
        };

        function getSettings() {
            if (_.size(treehouse.client.configuration.settings) > 0) {
                return treehouse.client.configuration.settings;
            } else {
                return {
                    isDebugMode: true
                };
            }
        }

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('treehouse.data')
        .factory('coreHttpInterceptor', coreHttpInterceptor);

    coreHttpInterceptor.$inject = ['loggerSvc', '$q', 'applicationConfigSvc'];

    function coreHttpInterceptor(loggerSvc, $q, applicationConfigSvc) {
        var logger = loggerSvc.get('coreHttpInterceptor');
        return {
            // optional method
            'request': function (config) {
                // do something on success
                logger.debug('Request sent', config);
                if (!_.endsWith(config.url, 'html') && applicationConfigSvc.settings.requestContext !== undefined) {
                    config.params = config.params || {};
                    config.params = $.extend({}, applicationConfigSvc.settings.requestContext, config.params);
                }
                return config;
            },
            // optional method
            'requestError': function (rejection) {
                // do something on error
                logger.error('Error occurred while sending request', rejection);
                return $q.reject(rejection);
            },
            // optional method
            'response': function (response) {
                // do something on success
                logger.debug('Response received ', response);
                if (!_.isNullOrUndefined(response)
                    && !_.isNullOrUndefined(response.data)
                    && !_.isNullOrUndefined(response.data.requestContext)) {
                    logger.debug('Response: retrieving context', response.data.requestContext);
                    applicationConfigSvc.settings.requestContext = response.data.requestContext;
                    response.data = response.data.result;
                }
                return response;
            },
            // optional method
            'responseError': function (rejection) {
                // do something on error
                logger.error('Error occurred while for response', rejection);
                return $q.reject(rejection);
            }
        };


    }
})();
(function () {
    'use strict';

    var module = angular
        .module('treehouse.data');

    module.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('coreHttpInterceptor');
    }]);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.data')
        .provider('repoBaseConfig', repoBaseConfig)
        .service('repoBaseSvc', repoBaseSvc);

    function repoBaseConfig() {
        this.config = {

        };

        this.$get = function () {
            return {
                config: this.config
            };
        }
    };
    repoBaseSvc.$inject = ['$q', '$http', 'repoBaseConfig', 'resolveUrlFilter', 'loggerSvc', 'Upload'];


    function repoBaseSvc($q, $http, repoBaseConfigProvider, resolveUrl, loggerSvc, uploadSvc) {
        var logger = loggerSvc.get('RepoBaseSvc'),
            buildUrl = function (baseUrl, id) {
                var url = resolveUrl(repoBaseConfigProvider.config.baseUrl + baseUrl);
                if (!_.isNullOrUndefined(id)) {
                    url += '/' + id;
                }
                return url;
            },
            iframe = null,
            contract = {
                service: service
            };
        function ContentAbstractSvc(routeConfig) {
            this._baseUrl = routeConfig;
        };

        function processResult(httpPromise) {
            var defer = $q.defer();
            httpPromise.success(function (data, status, headers, config) {
                defer.resolve(data);
            }).error(function (data, status, headers, config) {
                defer.reject({ data: data, status: status, headers: headers, config: config });
            });;
            return defer.promise;
        }

        ContentAbstractSvc.prototype = {
            getList: function (params) {
                var url = buildUrl(this._baseUrl, null);
                logger.debug('Get List Called for: ' + url);
                return processResult($http.get(url, { params: params }));
            },
            get: function (id, params) {
                var url = buildUrl(this._baseUrl, id);
                logger.debug('Get Called for: ' + url);
                return processResult($http.get(url, { params: params }));
            },
            create: function (item, params) {
                var url = buildUrl(this._baseUrl, null);
                logger.debug('Created Called for: ' + url);
                return processResult($http.post(url, item, { params: params }));
            },
            save: function (item, params) {
                var url = buildUrl(this._baseUrl, null);
                logger.debug('Save Called for: ' + url);
                return processResult($http.put(url, item, { params: params }));
            },
            remove: function (id, params) {
                var url = buildUrl(this._baseUrl, id);
                logger.debug('remove Called for: ' + url);
                return processResult($http.delete(url, { params: params }));
            },
            saveWithFile: function (file, model) {
                var url = buildUrl(this._baseUrl, null);
                logger.debug('Save File Called for: ' + url);
                return uploadUsing$upload(file, model, 'PUT', url);
            },
            createWithFile: function (file, model) {
                var url = buildUrl(this._baseUrl, null);
                logger.debug('Create File Called for: ' + url);
                return uploadUsing$upload(file, model, 'POST', url);
            },
            downloadFile: function (id) {
                var url = buildUrl(this._baseUrl, id);
                logger.debug('download File Called for: ' + url);
                createIframe(url);
            }
        }



        function service(endPoint) {
            return new ContentAbstractSvc(endPoint);
        }


        function uploadUsing$upload(file, model, method, url) {
            method = method || 'POST';
            model = model || {};
            var defer = $q.defer();

            logger.debug('uploadUsing$upload called: ' + url);

            file.upload = uploadSvc.upload({
                url: url,
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: model,
                file: file,
                fileFormDataName: 'bmUploadedFiles'
            });

            file.upload.then(function (response) {
                defer.resolve(response.data);
            }, function (response) {
                defer.reject(response);
                //if (response.status > 0)
                //    $scope.errorMsg = response.status + ': ' + response.data;
            });

            file.upload.progress(function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                logger.debug('File upload progress ' + file.progress, file.progress);
            });

            file.upload.xhr(function (xhr) {
                // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
            });

            return defer.promise;
        }


        function createIframe(url) {
            if (iframe === null) {
                iframe = $('<iframe style="display: none;"></iframe>');
                iframe.appendTo($("body"));
            }
            if (url.indexOf('?') > -1) {
                url = url + '&';
            } else {
                url = url + '?';
            }
            url = url + "randDownloadKey=" + treehouse.client.util.getRandomKey();

            iframe.attr('src', url);
        }



        return contract;
    }
})();
(function () {
    'use strict';
    var module = angular.module('treehouse.diagnostics', []);
    module.factory('loggerSvc', loggerSvc);
    loggerSvc.$inject = ['loggerFactory', 'applicationConfigSvc'];
    function loggerSvc(loggerFactory, applicationConfigSvc) {

        var contract = {
            get: get,
            getLogs: getLogs,
            saveLogs: saveLogs
        },
            logStructure = {
                logs: [],
                track: []
            },
            enableToaster = applicationConfigSvc.settings.isDebugMode;


        function get(id) {
            return loggerFactory.get(id, addLogs, enableToaster);
        }

        function getLogs(options) {
            return logStructure;
        }

        function saveLogs() {

        }

        function addLogs(log) {
            //Remove the older logs.
            if (logStructure.logs.length >= 200) {
                logStructure.logs = logStructure.logs.slice(100, logStructure.logs.length);
            }

            logStructure.logs.push(log);
            if (log.category === "error") {
                //Save to server
            }

        }

        return contract;


    }
})();
(function () {
    'use strict';

    angular
        .module('treehouse.diagnostics')
        .service('errorHandlerSvc', errorHandlerSvc);

    errorHandlerSvc.$inject = ['validationHelperSvc'];

    function errorHandlerSvc(validationHelperSvc) {
        return {
            catch: function (validationGroup, errorMessage) {
                return function (error) {
                    errorMessage = errorMessage || "An error occurred while performing last action";
                    validationHelperSvc.removeAll(validationGroup);
                    if (error.status === 400 || error.status === 401) {
                        var errors = JSON.parse(error.data.message);
                        validationHelperSvc.add(errors, validationGroup);
                    }
                    else {
                        validationHelperSvc.add(errorMessage, validationGroup);
                    }
                };
            }
        };
    }
})();
(function (module) {

    module.config(['$provide', function ($provide) {
        $provide.decorator("$interpolate", ['$delegate', 'loggerSvc', function ($delegate, loggerSvc) {

            var serviceWrapper = function () {
                var bindingFn = $delegate.apply(this, arguments);
                if (angular.isFunction(bindingFn) && arguments[0]) {
                    return bindingWrapper(bindingFn, arguments[0].trim());
                }
                return bindingFn;
            }, logger = loggerSvc.get('InterpolateDebug');

            var bindingWrapper = function (bindingFn, bindingExpression) {
                return function () {
                    var result = bindingFn.apply(this, arguments);
                    var trimmedResult = result.trim();
                    if (trimmedResult === "") {
                        sendWarning(bindingExpression, trimmedResult);
                        //var errorMsg = bindingExpression + " = " + trimmedResult;
                        //logger.warn("Possible Binding error" + errorMsg, errorMsg, true);
                    }

                    return result;
                };
            };
            var existingWarning = {};
            function sendWarning(bindingExpression, trimmedResult) {
                if (existingWarning[bindingExpression] === undefined) {
                    existingWarning[bindingExpression] = trimmedResult;
                    var errorMsg = bindingExpression + " = " + trimmedResult;
                    logger.warn("Possible Binding error" + errorMsg, errorMsg, true);
                }

                if (_.size(existingWarning) > 20) {
                    existingWarning = {};
                }

            }
            angular.extend(serviceWrapper, $delegate);
            return serviceWrapper;

        }]);
    }]);

}(angular.module("treehouse.diagnostics")));

(function () {
    'use strict';

    angular
        .module('treehouse.diagnostics')
        .service('loggerFactory', loggerFactory);

    loggerFactory.$inject = ['$log', '$window'];

    function loggerFactory($log, $window) {
        // Configure Toastr
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';

        var service = {
            get: get
        }
        function get(id, addLogCallBack, enableToast) {
            return new Logger(id, addLogCallBack, enableToast);
        }

        return service;

        function Logger(id, logStore, enableToast) {
            var service = {
                error: error,
                info: info,
                debug: debug,
                success: success,
                warn: warning,
                track: track
            },
                categoryType = {
                    error: "error",
                    info: "info",
                    debug: "debug",
                    success: "success",
                    warn: "warning",
                    track: "track"
                },
                toastTypes = {
                    "debug": toastr.info,
                    "info": toastr.info,
                    "success": toastr.success,
                    "warning": toastr.warning,
                    "error": toastr.error
                },
                logTypes = {
                    "debug": $log.info,
                    "info": $log.info,
                    "success": $log.success,
                    "warning": $log.warn,
                    "error": $log.error
                };


            function buildArgs(category, origialArguments) {
                var args = Array.prototype.slice.call(origialArguments);
                args = [category].concat(args);
                return args;
            }
            function error(message, data, showToast) {
                var args = buildArgs(categoryType.error, arguments);
                log.apply(this, args);
            }

            function info(message, data, showToast) {
                var args = buildArgs(categoryType.info, arguments);
                log.apply(this, args);
            }


            function debug(message, data, showToast) {
                var args = buildArgs(categoryType.debug, arguments);
                log.apply(this, args);
            }

            function success(message, data, showToast) {
                var args = buildArgs(categoryType.success, arguments);
                log.apply(this, args);
            }

            function warning(message, data, showToast) {
                var args = buildArgs(categoryType.warn, arguments);
                log.apply(this, args);
            }

            function track() {
                var args = buildArgs(categoryType.track, arguments);
                log.apply(this, args);
            }

            function saveLogs(message, data) {
                logStore.logs.push({ messsage: message, data: data, category: id });
            }

            function getToastr(category) {
                var toast = toastTypes[category];
                if (toast === undefined) {
                    toast = toastr.info;
                }
                return toast;
            }

            function getNgLogger(category) {
                var logger = logTypes[category];
                if (logger === undefined) {
                    logger = $log.info;
                }
                return logger;
            }

            function getTimeStamp() {
                var now = new Date();
                now = new Date(
                    now.getUTCFullYear(),
                    now.getUTCMonth(),
                    now.getUTCDate(),
                    now.getHours(),
                    now.getMinutes(),
                    now.getSeconds(),
                    now.getMilliseconds());
                return now;
            }

            function log(category, message, data, showToast) {
                var toast = getToastr(category),
                    ngLogger = getNgLogger(category);

                if ((showToast || category === "error") && enableToast) {
                    toast(message, id);

                }

                if (enableToast) {
                    ngLogger(message, data);
                }

                logStore({ id: id, category: category, message: message, data: data, timeStamp: getTimeStamp(), savedToServer: false, url: $window.location.href });

            }
            return service;
        }

    }
})();
(function () {
    'use strict';
    angular.module('treehouse.form', ['treehouse.diagnostics']);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .directive('bmFormContainer', bmFormContainer);

    bmFormContainer.$inject = ['validationHelperSvc'];

    function bmFormContainer(validationHelperSvc) {
        // Usage:
        //     <form data-bm-form-container="" >
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                validationGroup: "="
            },
            controller: ['$scope', '$element', controller]
        };
        return directive;

        function controller(scope, element) {
            var validationsCallBacks = [];
            this.enableValidations = function () {
                element.addClass('enable-validation');
                notifyValidationStatus(true);
            }

            this.disableValidations = function () {
                element.removeClass('enable-validation');
                notifyValidationStatus(false);
            }

            this.updateSummary = function (form, name, displayName, lastError, sortId) {
                var group = scope.validationGroup || 'root';
                return validationHelperSvc.updateSummary(form, name, displayName, lastError, sortId, group);
            }

            this.onValidationsStatusChanged = function (callBack) {
                validationsCallBacks.push(callBack);
            }

            function notifyValidationStatus(isEnabled) {
                angular.forEach(validationsCallBacks, function (callBack) {
                    callBack(isEnabled);
                });
            }
        }

        function link(scope, element, attrs) {
            if (attrs.novalidate === undefined) {
                element.attr("novalidate", "");
            }
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .directive('bmFormSubmitValidator', bmFormSubmitValidator);

    bmFormSubmitValidator.$inject = ['$window'];

    function bmFormSubmitValidator($window) {
        // Usage:
        //   <input type="submit" data-bm-form-submit-validator=""/>
        //   <button data-bm-form-submit-validator=""></button>
        // Creates:
        // 
        var directive = {
            link: link,
            require: ['^form', "^bmFormContainer"],
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs, ctrls) {
            var form = ctrls[0];
            var formContainer = ctrls[1];
            element.click(function (event) {
                if (form.$invalid) {
                    event.preventDefault();
                    formContainer.enableValidations();
                } else {
                    formContainer.disableValidations();
                }
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .directive('bmFormValidationField', bmFormValidationField);

    bmFormValidationField.$inject = ['commonSvc'];

    function bmFormValidationField(commonSvc) {
        // Usage:
        //     <input data-bm-form-validation-field=""/>
        // Creates:
        // 
        var directive = {
            link: link,
            require: ["^form", "^bmFormContainer"],
            restrict: 'A'
        },
            uniqueId = 0,
            getUnqiueId = function () {
                uniqueId = uniqueId + 1;
                return uniqueId;
            };
        return directive;
        function watchFor(form, name) {
            return function () {
                var item = form[name];
                if (item && item.$error) {
                    var rtn = "";
                    var object = form[name].$error;
                    for (var key in object) {
                        if (object.hasOwnProperty(key)) {
                            rtn += key;
                        }
                    }
                    return rtn;
                }
            };
        }

        function validateSetup(form, formContainer, attrs) {
            if (attrs.name === undefined) {
                throw 'bmFormValidationField must have name attribute defined';
            }

            if (form.$name === undefined) {
                throw 'bmFormValidationField requires that form containing the input should have name assigned';
            }

            if (formContainer === undefined) {
                throw 'bmFormValidationField requires form container.';
            }
        }

        function link(scope, element, attrs, ctrls) {
            var form = ctrls[0],
                formContainer = ctrls[1],
                name = attrs.name,
                displayName = attrs.friendlyName,
                lastError = {},
                sortId = getUnqiueId(),
                validateOnWatch;

            validateSetup(form, formContainer, attrs);

            formContainer.onValidationsStatusChanged(function (isEnabled) {
                commonSvc.safeApply(scope, function () {
                    if (isEnabled === true) {
                        validateOnWatch = scope.$watch(watchFor(form, name), function () {
                            lastError = formContainer.updateSummary(form, name, displayName, lastError, sortId);
                        });
                    } else if (isEnabled === false && typeof validateOnWatch == "function") {
                        validateOnWatch();
                    }
                });
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .directive('bmValidationMessages', bmValidationMessages);

    bmValidationMessages.$inject = ['$templateRequest', 'validationHelperSvc', 'validationHelperConfig'];

    function bmValidationMessages($templateRequest, validationHelperSvc, validationHelperConfig) {
        // Usage:
        //     <bmValidationMessages></bmValidationMessages>
        // Creates:
        // 
        var directive = {
            link: link,
            require: "^form",
            restrict: 'EA'
        };
        return directive;
        function validateSetup(element, attrs) {
            if (attrs.targetField === undefined) {
                throw "bmValidationMessages does not have a targetField attribute defined";
            }
        }
        function setValidations(key, html) {
            var validations = validationHelperConfig.config.getValdationConfig(html);
            validationHelperSvc.setInputValidationConfig(key, validations);
        }

        function link(scope, element, attrs, form) {
            element.hide();
            validateSetup(element, attrs);
            var inputKey = form.$name + "." + attrs.targetField,
                tpl = attrs.includeTemplate;
            if (tpl !== undefined) {
                $templateRequest(tpl)
                    .then(function processTemplate(html) {
                        setValidations(inputKey, html);
                    });
            } else {
                var innerHtml = $(element).html();
                setValidations(inputKey, innerHtml);
            }
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .directive('bmValidationSummary', bmValidationSummary);

    bmValidationSummary.$inject = ['validationHelperSvc'];

    function bmValidationSummary(validationHelperSvc) {
        // Usage:
        //     <bmValidationSummary></bmValidationSummary>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            template: "<div data-ng-show='validationListing.length>0' data-ng-animate='' class='alert alert-danger zoomin-animation'>" +
                "<p class='hanging-icon-indent' data-bm-scroll-in-view='validationListing.length>0'>" +
                "<span class='fa fa-exclamation-circle'></span>" +
                "The following error(s) were found:" +
                "</p>" +
                "<ul><li data-ng-repeat='messageConfig in validationListing' ng-bind-html='messageConfig.message'></li></ul>" +
                "</div>"
        };
        return directive;

        function link(scope, element, attrs) {
            var group;
            scope.$evalAsync(function () {
                group = scope.$eval(attrs.validationGroup) || 'root';
                scope.validationListing = validationHelperSvc.getValidationListing(group);
            });

            scope.$on('$destroy', function cleanup() {
                validationHelperSvc.removeAll(group);
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.form')
        .provider('validationHelperConfig', validationHelperConfig)
        .factory('validationHelperSvc', validationHelperSvc);

    function validationHelperConfig() {
        var defaultValidations = [
            {
                errorKey: "required",
                message: "#FRIENDLYNAME# is required."
            }
            , {
                errorKey: "pattern",
                message: "#FRIENDLYNAME# is invalid."
            }, {
                errorKey: "bmPattern",
                message: "#FRIENDLYNAME# is invalid."
            }, {
                errorKey: "email",
                message: "#FRIENDLYNAME# is invalid."
            }
            , {
                errorKey: "minlength",
                message: "#FRIENDLYNAME# is to small."
            }
        ];

        this.config = {
            supportedValidations: defaultValidations,
            getValdationConfig: function (html) {
                var elems = $.parseHTML(html),
                    validations = [];
                $.each(elems, function (i, el) {
                    el = $(el);
                    var ngMessage = el.data('ng-message') || el.attr('ng-message');
                    if (ngMessage !== undefined) {
                        validations.push({ message: el.html(), errorKey: ngMessage });
                    }
                });
                return validations;
            },
            setValidationConfig: function (html) {
                this.supportedValidations = this.getValdationConfig(html);
            }
        };
        this.$get = function () {
            return {
                config: this.config
            }
        }
    }

    validationHelperSvc.$inject = ['validationHelperConfig'];

    function validationHelperSvc(validationHelperConfigProvider) {
        var service = {
            setInputValidationConfig: setInputValidationConfig,
            updateSummary: updateSummary,
            getValidationListing: getValidationListing,
            add: add,
            remove: remove,
            removeAll: removeAll
        },
            validationListing = {},
            inputValidations = {};

        function add(errors, validationGroup, key) {
            validationGroup = validationGroup || 'root';
            initalizeValidationGroup(validationGroup);
            //check if any errors were passed.
            var length = _.size(errors);
            if (length === 0) {
                return;
            }

            if (typeof errors == "string" && length > 0) {
                errors = [errors];
            }
            key = key || 'key_';
            for (var i = 0, l = errors.length; i < l; i++) {
                var error = errors[i],
                    id = _.uniqueId();
                key = key + id;

                addValdiation(key, error, id, validationGroup);
            }
        }

        function remove(key, validationGroup) {
            validationGroup = validationGroup || 'root';
            removeValidation(key, validationGroup);
        }

        function removeAll(validationGroup) {
            validationGroup = validationGroup || 'root';
            var arr = validationListing[validationGroup];
            if (arr !== undefined && arr.length > 0) {
                arr.splice(0, arr.length);
            }
        }

        function setInputValidationConfig(inputKey, validationConfigs) {
            inputValidations[inputKey.toLowerCase()] = validationConfigs;
        }

        function initalizeValidationGroup(group) {
            if (validationListing[group] === undefined) {
                validationListing[group] = [];
            }
        }
        function getValidationListing(group) {
            initalizeValidationGroup(group);

            return validationListing[group];
        }

        function getValidationConfigs(key) {
            var validations = inputValidations[key];
            if (validations === undefined) {
                validations = validationHelperConfigProvider.config.supportedValidations;
            } else if (inputValidations[key].isMerged === undefined) {
                var mergedValdiations = [];
                _.each(validationHelperConfigProvider.config.supportedValidations, function (val) {
                    var userDefinedValidation = _.findWhere(validations, { errorKey: val.errorKey });
                    if (userDefinedValidation === undefined) {
                        mergedValdiations.push(val);
                    } else {
                        mergedValdiations.push(userDefinedValidation);
                    }
                });
                validations = mergedValdiations;
                inputValidations[key] = mergedValdiations;
                inputValidations[key].isMerged = true;
            }
            return validations;
        }

        function getCurrrentError(key, $errors) {
            var validations = getValidationConfigs(key);
            for (var index = 0, len = validations.length; index < len; index++) {
                var msg = validations[index];
                if ($errors[msg.errorKey] !== undefined) {
                    return msg;
                }
            }
        }

        function addValdiation(validationKey, message, sortId, group) {
            var index = getIndexByKey(validationKey, group),
                listing = validationListing[group];

            if (index === -1) {
                listing.push({ validationKey: validationKey, message: message, sortId: sortId });
                listing.sort(function (a, b) {
                    return a.sortId > b.sortId;
                });
            } else {
                listing[index].message = message;
            }
        }

        function removeValidation(validationKey, group) {
            var index = getIndexByKey(validationKey, group);
            if (index > -1) {
                validationListing[group].splice(index, 1);
            }
        }

        function getIndexByKey(validationKey, group) {
            var index = -1;
            for (var i = 0, len = validationListing[group].length; i < len; i++) {
                if (validationListing[group][i].validationKey.toLowerCase() == validationKey.toLowerCase()) {
                    index = i;
                    break;
                }
            }
            return index;
        }
        function buildValidationKey(formName, elementName) {
            var k = formName + '.' + elementName;
            return k.toLowerCase();
        }

        function removeOldErrors(formName, elementName, lastErrors, currentErrors, group) {
            for (var lastKey in lastErrors) {
                if (currentErrors[lastKey] === undefined) {
                    var key = buildValidationKey(formName, elementName);
                    removeValidation(key, group);
                }
            }
        }

        function updateSummary(form, name, displayName, lastError, sortId, group) {
            group = group || 'root';

            var $errors = form[name].$error;
            var key = buildValidationKey(form.$name, name);

            removeOldErrors(form.$name, name, lastError, $errors, group);

            var currentError = getCurrrentError(key, $errors);
            if (currentError != null) {
                var message = currentError.message.replace('#FRIENDLYNAME#', displayName);
                addValdiation(key, message, sortId, group);
            }
            return $.extend({}, $errors, true);

        }


        return service;

    }
})();
(function () {
    'use strict';


    var moduleId = "treehouse.modal";


    angular.module("bm.modal.standard.template", []).run([
        "$templateCache", function ($templateCache) {
            $templateCache.put("template/bmModal/standard.html",
                '<div ng-multi-transclude-controller="" data-backdrop="static" data-keyboard="false" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
                '<div class="modal-dialog  {{modalClass}}">' +
                '<div class="modal-content">' +

                        '<div class="modal-header">' +
                        '<button ng-hide="showSpiny" type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '<h4 class="modal-title" id="myModalLabel">{{title}}</h4>' +
                        '</div>' +
                        '<div data-bm-indicator="showSpiny">' +
                        '<div class="modal-body">' +
                        '<div ng-multi-transclude="body"></div>' +
                        '</div>' +
                        '<div class="modal-footer" ng-multi-transclude="footer"></div>' +
                '</div>' +

                    '</div>' +
                '</div>' +
                '</div>');
        }
    ]);


    var modalModule = angular.module(moduleId, ["bm.modal.standard.template", 'multi-transclude', 'ui.bootstrap', 'bm.modal.confirm.template']);

    modalModule.factory("bmModalSvc", ['$q', '$document', '$compile', '$rootScope', 'bmNotifierSvc',
        function ($q, $document, $compile, $rootScope, notifier) {
            var service = {
                confirm: confirm
            },
            isAppended = false,
            confirmTemplate =
                 '<div data-ng-controller="bmModalDialogCtrl as modalCtrl">' +
                    '<div data-bm-modal="" data-open-if="modalCtrl.showDialog" data-title="\'Confirm\'" data-modal-class="\'modal-300\'">' +
                       '<div data-section="body" style="text-align: center">' +
                       '{{modalCtrl.message}}' +
                       '</div>' +
                       '<div data-section="footer">' +
                       '<button type="button"  data-ng-click="modalCtrl.cancel()" class="btn btn-default" data-dismiss="modal">Cancel</button>' +
                       '<button type="button" data-ng-click="modalCtrl.confirm()" class="btn btn-primary">OK</button>' +
                       '</div>' +
                       '</div>' +
                   '</div>';

            function confirm(config) {
                var defer = $q.defer();
                if (!isAppended) {
                    var scope = $rootScope.$new();
                    var dialog = $compile(confirmTemplate)(scope);
                    $document.find('body').append(dialog);
                    isAppended = true;
                }

                notifier.onChangeSubscribe('CONFIRM-RESULT', function (result) {
                    if (result === true) {
                        defer.resolve();
                    } else {
                        defer.reject();
                    }
                });

                notifier.onChangeNotify('CONFIRM', config.message);

                return defer.promise;
            }

            return service;
        }
    ]);

    modalModule.controller("bmModalDialogCtrl", ['bmModalSvc', 'bmNotifierSvc', '$scope', function (bmModalSvc, notifier, $scope) {
        var modalCtrl = this;
        modalCtrl.showDialog = false;
        activate();
        function activate() {
            notifier.onChangeSubscribe('CONFIRM', function (msg) {
                safeApply($scope, function () {
                    modalCtrl.showDialog = true;
                    modalCtrl.message = msg;
                });

            });
        }

        modalCtrl.cancel = function () {
            modalCtrl.showDialog = false;
            notifier.onChangeNotify('CONFIRM-RESULT', false);
        }

        modalCtrl.confirm = function () {
            modalCtrl.showDialog = false;
            notifier.onChangeNotify('CONFIRM-RESULT', true);
        }
    }
    ]);
    modalModule.directive("bmModal", [
        function () {
            return {
                restrict: 'A',
                templateUrl: "template/bmModal/standard.html",
                transclude: true,
                scope: {
                    openIf: "=",
                    title: "=",
                    onClose: "=",
                    modalClass: "=",
                    showSpiny: "="
                },
                link: function (scope, element) {
                    var modalContainer = $(element).find("#myModal");

                    modalContainer.on('hidden.bs.modal', function () {
                        safeApply(scope, function () {
                            if (typeof scope.onClose == "function") {
                                scope.onClose();
                            }
                            scope.openIf = false;
                        });
                    });

                    scope.$watch(function () {
                        if (scope.openIf === true) {
                            return true;
                        }
                        return false;
                    }, function (openIf) {
                        if (openIf === true) {
                            modalContainer.modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            modalContainer.appendTo("body").modal('show');
                        } else if (openIf === false && modalContainer.modal) {
                            modalContainer.modal('hide');
                        }
                    });
                }
            };
        }
    ]);

    modalModule.factory("bmNotifierSvc", [function () {
        var notifier = (function () {
            var subscribers = {},
                getEvent = function (eventType) {
                    var event = subscribers[eventType];
                    if (event === undefined) {
                        event = [];
                        subscribers[eventType] = event;
                    }
                    return event;
                };

            return {
                onChangeNotify: function (eventType, args) {
                    var event = getEvent(eventType);
                    for (var i = 0, l = event.length; i < l; i++) {
                        event[i](args);
                    }
                },
                onChangeSubscribe: function (eventType, callBack) {
                    var event = getEvent(eventType);
                    event.push(callBack);
                }
            };
        })();

        return notifier;
    }
    ]);

    function safeApply(scope, fn) {
        var phase = scope.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            scope.$apply(fn);
        }
    }
}());

(function () {
    var module = angular.module('multi-transclude', []);

    var Ctrl = ['$scope', '$element', '$transclude', function ($scope, $element, $transclude) {
        // Ensure we're transcluding or nothing will work.
        if (!$transclude) {
            throw new Error(
              'Illegal use of ngMultiTransclude controller. No directive ' +
              'that requires a transclusion found.'
            );
        }

        var toTransclude;

        $scope.$on('$destroy', function () {
            if (toTransclude) {
                toTransclude.remove();
                toTransclude = null;
            }
        });

        // Transclude content that matches name into element.
        this.transclude = function (name, element) {
            for (var i = 0; i < toTransclude.length; ++i) {
                // Uses the argument as the `name` attribute directly, but we could
                // evaluate it or interpolate it or whatever.
                var el = angular.element(toTransclude[i]);
                if (el.attr('data-section') === name) {
                    element.append(el);
                    return;
                }
            }
        };

        // There's not a good way to ask Angular to give you the closest
        // controller from a list of controllers, we get all multi-transclude
        // controllers and select the one that is the child of the other.
        this.$element = $element;
        this.isChildOf = function (otherCtrl) {
            return otherCtrl.$element[0].contains(this.$element[0]);
        };

        // get content to transclude
        $transclude(function (clone) {
            toTransclude = clone;
        });
    }];

    module.directive('ngMultiTemplate', function () {
        return {
            transclude: true,
            templateUrl: function (element, attrs) {
                return attrs.ngMultiTemplate;
            },
            controller: Ctrl
        };
    });

    module.directive('ngMultiTranscludeController', function () {
        return {
            controller: Ctrl
        };
    });

    module.directive('ngMultiTransclude', function () {
        return {
            require: ['?^ngMultiTranscludeController', '?^ngMultiTemplate'],
            link: function (scope, element, attrs, ctrls) {
                // Find the deepest controller (closes to this element).
                var ctrl1 = ctrls[0];
                var ctrl2 = ctrls[1];
                var ctrl;
                if (ctrl1 && ctrl2) {
                    ctrl = ctrl1.isChildOf(ctrl2) ? ctrl1 : ctrl2;
                }
                else {
                    ctrl = ctrl1 || ctrl2;
                }

                // A multi-transclude parent directive must be present.
                if (!ctrl) {
                    throw new Error('Illegal use of ngMultiTransclude. No wrapping controller.')
                }

                // Receive transcluded content.
                ctrl.transclude(attrs.ngMultiTransclude, element);
            }
        };
    });


})();
(function () {
    'use strict';
    // angular.module('treehouse.modal', []);
    angular
        .module('treehouse.modal')
        .factory('dialogueSvc', dialogueSvc);

    dialogueSvc.$inject = ['$modal'];

    function dialogueSvc($modal) {
        var service = {
            confirm: confirm
        };

        var modalDefaults = {
            backdrop: true,
            keyboard: false,
            modalFade: true,
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            title: 'Proceed?',
            message: 'Perform this action?',
            templateUrl: 'template/bmModal/confirm.html'
        };
        function confirm(customModalDefaults) {


            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {

                    $scope.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.cancel = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

        return service;

    }

    angular.module("bm.modal.confirm.template", []).run([
      "$templateCache", function ($templateCache) {
          $templateCache.put("template/bmModal/confirm.html",
              '<div data-vin tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal"  data-ng-click="$dismiss($event)">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '<span class="sr-only">Close</span>' +
                        '</button>' +
                        '<h4 class="modal-title">{{title}}</h4>' +
                    '</div>' +

                    '<div class="modal-body" style="min-height:200px;">' +
                     '  Some text goes here' +
                    '</div>' +
                   '<div class="modal-footer">' +
                       ' <button type="button" class="btn btn-default" data-ng-click="cancel()" data-dismiss="modal">Cancel</button>' +
                        '<button type="submit" class="btn btn-primary" data-ng-click="ok()">OK</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>');
      }
    ]);

})();
(function () {
    'use strict';

    angular.module('treehouse.router', ['treehouse.diagnostics', 'ui.router']);

})();
(function () {
    'use strict';

    angular
        .module('treehouse.router')
        .factory('routeHelper', routeHelper)
        .provider('routeHelperConfig', routeHelperConfig);

    function routeHelperConfig() {
        this.config = {
            // These are the properties we need to set
            // $stateProvider: undefined
            // docTitle: ''
        }

        this.$get = function () {
            return {
                config: this.config
            }
        }
    }
    routeHelper.$inject = ['$rootScope', 'routeHelperConfig', 'commonSvc', '$state'];

    function routeHelper($rootScope, routeHelperConfigProvider, common, $state) {

        var service = {
            configureRoutes: configureRoutes,
            configureRouteSecurity: configureRouteSecurity,
            configurePageTitle: configurePageTitle,
            configureRouteChangeError: configureRouteChangeError
        },
            $stateProvider = routeHelperConfigProvider.config.$stateProvider,
            $urlRouterProvider = routeHelperConfigProvider.config.$urlRouterProvider,
            defaultRoute = routeHelperConfigProvider.config.defaultRoute,
                logger = common.logger.get('routeHelper');

        function configureRoutes(routes) {
            _.each(routes, function (route) {
                $stateProvider.state(route.state, route.config);
            });
            $urlRouterProvider.otherwise(defaultRoute);
        }


        function configureRouteSecurity() {
            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    logger.debug('Navigated from "' + fromState.name + '" to "' + toState.name + '"');
                    if (!_.isNullOrUndefined(toState.data)
                        && typeof toState.data.isAuthorized == "function"
                        && !toState.data.isAuthorized()) {
                        event.preventDefault();
                    }
                });
        }

        function configureRouteChangeError() {
            $rootScope.$on('$stateChangeError',
                function (event, toState, toParams, fromState, fromParams, error) {
                    logger.error('Error occurred while trying to navigate to "'
                        + toState.name + '" message: "' + error.message + '"', { toState: toState, error: error });
                    if (routeHelperConfigProvider.config.errorState !== undefined) {
                        $state.go(routeHelperConfigProvider.config.errorState);
                    }
                });
        }

        function configurePageTitle() {
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    logger.debug('Navigated to "' + toState.name + '"');
                    if (toState.data !== undefined) {
                        $rootScope.docTitle = routeHelperConfigProvider.config.docTitle + toState.data.title;
                    }
                });
        }

        return service;
    }
})();
(function () {
    'use strict';

    angular.module('treehouse.security', []);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.security')
        .directive('bmIsUserInRole', bmIsUserInRole);


    function bmIsUserInRole() {
        // Usage:
        //     <bmIsUserInRole></bmIsUserInRole>
        // Creates:
        // 
        var directive = {
            restrict: 'A',
            replace: true,
            template: function (element, attr) {
                var ngIf = attr.ngIf;
                var value = attr.bmIsUserInRole.indexOf('admin') > -1;
                /**
                 * Make sure to combine with existing ngIf!
                 */
                if (ngIf) {
                    value += ' && ' + ngIf;
                }
                var inner = element.get(0);
                //we have to clear all the values because angular
                //is going to merge the attrs collection 
                //back into the element after this function finishes
                angular.forEach(inner.attributes, function (attr, key) {
                    attr.value = '';
                });
                attr.$set('ng-if', value);
                return inner.outerHTML;
            }
        };
        return directive;
    }

})();
(function () {
    'use strict';

    angular.module('treehouse.ui', []);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmComboWithAdd', bmComboWithAdd);

    bmComboWithAdd.$inject = ['commonSvc'];

    function bmComboWithAdd(commonSvc) {
        // Usage:
        //     <bmComboWithAdd></bmComboWithAdd>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            require: 'ngModel'
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            $(element).select2({
                allowClear: true,
                formatNoMatches: function (term) {
                    /* customize the no matches output */

                    return '<div class="input-group"><input class="form-control" id="newTerm" value="'
                    + term + '"><span class="input-group-btn"><a href="javascript:void(0);" id="addNew" class="btn btn-primary">Create</a> </span>' +
                                '</div>';
                }
            }).parent().find('.select2-with-searchbox').on('click', '#addNew', function () {
                /* add the new term */
                var newTerm = toTitleCase($('#newTerm').val());
                //alert('adding:'+newTerm);
                $('<option>' + newTerm + '</option>').appendTo($(element));
                $(element).select2('val', newTerm); // select the new term
                $(element).select2('close');		// close the dropdown
                commonSvc.safeApply(scope, function () {
                    ngModelCtrl.$setViewValue(newTerm);
                });
            });

            ngModelCtrl.$formatters.push(function (num) {
                if (num !== undefined) {
                    $(element).select2('val', num);

                }
                return num;
            });

            function toTitleCase(str) {
                return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
            }
        }
    }

})();
(function () {
    'use strict';

    var app = angular
        .module('treehouse.ui');

    app.directive('bmDatePicker', bmDatePicker);

    bmDatePicker.$inject = ['defaultDateFilter'];

    function bmDatePicker(defaultDate) {
        // Usage:
        //     <bmDatePicker></bmDatePicker>
        // Creates:
        // 
        var directive = {
            link: link,
            require: 'ngModel',
            restrict: 'EA'
        };
        return directive;
        function link(scope, element, attrs, ngModelCtrl) {
            var updateModel = function () {
                scope.$apply(function () {
                    var num = datePickerBox.value();
                    ngModelCtrl.$setViewValue(num);
                    //if (!_.isNullOrUndefined(callBack) && callBack.length > 0) {
                    //    scope.$eval(callBack);
                    //}
                });
            },
                datePickerBox,
                defaultOptions = {
                    change: updateModel,
                    format: "MM/dd/yyyy"
                },
                userDefinedOptions = scope.$eval(attrs.options) || {},
                callBack = attrs.onChange;


            userDefinedOptions = angular.extend(userDefinedOptions, angular.extend({}, defaultOptions));
            //CREATE KENDO
            $(element).kendoDatePicker(userDefinedOptions);
            datePickerBox = $(element).data("kendoDatePicker");

            ngModelCtrl.$formatters.push(function (num) {
                if (num !== null) {
                    datePickerBox.value(num);
                }
                return num;
            });

            //if (!_.isNullOrUndefined(attrs.ngDisabled)) {
            //    scope.$watch(attrs.ngDisabled, function (isDisabled) {
            //        datePickerBox.enable(!isDisabled);
            //    });
            //}

            //if (!_.isNullOrUndefined(attrs.isEnabled)) {
            //    scope.$watch(attrs.isEnabled, function (isEnabled) {
            //        datePickerBox.enable(isEnabled);
            //    });
            //}

            scope.$on("$destroy", function () {
                if (datePickerBox != null && datePickerBox.destroy) {
                    datePickerBox.destroy();
                }
            });

        }
    }

})();
(function () {
    'use strict';

    var app = angular
        .module('treehouse.ui');

    app.directive('bmPatternValidator', bmPatternValidator);

    bmPatternValidator.$inject = ['$window'];

    function bmPatternValidator($window) {
        // Usage:
        //     
        // Creates:
        // 
        var directive = {
            link: link,
            require: "ngModel",
            restrict: 'A'
        },
            patterns = {
                date: '[0-9]{2}/[0-9]{2}/[0-9]{4}',
                number: '^(0|[1-9][0-9]*)$',
                phone: '^(\\([\\d]{3}\\) [\\d]{3}-\\d{4})?$'

            };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var patternKey = attrs.bmPatternValidator;
            if (patternKey === "") {
                throw 'bmPatternValidator: please assign a pattern key';
            }
            var pattern = patterns[patternKey];
            if (pattern === undefined) {
                throw 'bmPatternValidator: invalid pattern key: ' + patternKey;
            }
            var regex = new RegExp(pattern);
            ngModelCtrl.$validators.bmPattern = function (modelValue) {
                if (modelValue !== undefined) {
                    return regex.test(modelValue);
                }
                return true;
            }

        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmDataTable', bmDataTable);

    bmDataTable.$inject = ['bmDataTableSvc', 'commonSvc', 'bmTableConfig'];

    function bmDataTable(bmDataTableSvc, common, bmTableConfig) {
        // Usage:
        //     <bmDataTable></bmDataTable>
        // Creates:
        // 
        var directive = {
            link: link,
            scope: {
                enableSearch: "=",
                enablePaging: "=",
                config: "=",
                pageSize: "=",
                emptyMessage: "=",
                itemDatasource: "=",
                xEditableConfigs: "=",
                onCommand: "&"
            },
            restrict: 'EA'
        };
        return directive;
        function buildDom(scope, element, dataSource, settings, callBack) {
            var dtTable,
                domSettings = 'lrtip',
                tableBody,
                tableId = element.attr('id'),
                searchBox = null;
            if (settings.showSearch) {
                searchBox = $('<input class="form-control searchbox" placeholder="Search" style="width:300px; margin-bottom:6px;" type="text" data-search=""/>');
                element.before(searchBox);
                searchBox.keyup(function () {
                    dtTable.search($(this).val()).draw();
                });
            }

            if (settings.showPaging === false) {
                domSettings = "lrti";
            }

            var controlConfig = {
                "data": dataSource,
                "dom": domSettings,
                "bPaginate": settings.showPaging,
                "info": settings.showPaging,
                "columns": settings.config.columns,
                "columnDefs": settings.config.columnDefs,
                "displayLength": settings.pageSize,
                "language": {
                    "emptyTable": settings.emptyTableMsg,
                },
                "rowCallback": function (row, data, index) {

                    function onSuccess(response, newValue) {
                        var rowobj = dtTable.row(row);
                        rowobj.invalidate('data').draw(false);
                        var returnArgs = $.extend({ btn: btn }, { commandName: commandName }, { row: rowobj, data: data, index: index }, { xEditableValue: newValue });
                        callBack(returnArgs);

                    }

                    if (settings.xEditableConfigs.length > 0) {

                        var item = $('td', row).find("[data-bm-x-editable-command]"),
                            commandName = item.data('bm-x-editable-command'),
                            btn = item.find('a');
                        var config = _.findWhere(settings.xEditableConfigs, { commandName: commandName });
                        var mergedConfig = $.extend({}, { success: onSuccess }, config);


                        if (mergedConfig.type === "select" && !_.isNullOrUndefined(mergedConfig.selectedValueKey)) {
                            mergedConfig.value = data[mergedConfig.selectedValueKey];
                        }

                        item.editable(mergedConfig);
                    }
                }
            };
            controlConfig = $.extend(true, {}, bmTableConfig.config, controlConfig);
            element.html('<table class="display table table-striped table-bordered" cellspacing="0" width="100%"></table>');
            element.attr('id', tableId);
            dtTable = element.DataTable(controlConfig);
            tableBody = element.find('tbody');

            tableBody.on('click', 'a[data-command]', function () {
                var btn = $(this),
                    commandName = btn.attr('data-command');
                processAction($(this), dtTable, { commandName: commandName }, callBack);
            });

            function processAction(btn, dTable, actionFlags, onActionCallBack, xEditableResult) {
                xEditableResult = xEditableResult || {};
                var args = getRowInfo(btn, dTable),
                    returnArgs = $.extend({ btn: btn }, actionFlags, args, xEditableResult);
                onActionCallBack(returnArgs);
            }

            scope.$on('$destroy', function cleanup() {
                bmDataTableSvc.unRegister(tableId);
                tableBody.off('click', 'a[data-command]');
                if (dtTable.destroy) {
                    dtTable.clear();
                    dtTable.destroy(true);
                }
                dtTable = undefined;
                tableBody = undefined;
                element.remove();
            });
            return dtTable;
        }

        function getRowInfo(btn, dTable) {
            var row = dTable.row(btn.parents('tr')),
                data = row.data(),
                index = row.index();

            return {
                row: row,
                data: data,
                index: index
            };
        }

        function validateSetup(element, attrs) {
            if (attrs.id === undefined) {
                throw "bmDataTableSvc requires id to be defined.";
            }
        }

        function link(scope, element, attrs) {
            var grid,
                callBackOnCommand,
                settings;

            validateSetup(element, attrs);

            scope.$watch(function () {
                return scope.itemDatasource;
            }, function (dataSource) {
                if (grid === undefined && dataSource !== undefined) {
                    callBackOnCommand = scope.onCommand,
                        settings = {
                            showSearch: true,
                            showPaging: true,
                            pageSize: scope.pageSize || 10,
                            emptyTableMsg: scope.emptyMessage || "No items available to display.",
                            config: scope.config,
                            xEditableConfigs: scope.xEditableConfigs || []
                        };
                    if (scope.enableSearch === false) {
                        settings.showSearch = false;
                    }
                    if (scope.enablePaging === false) {
                        settings.showPaging = false;
                    }
                    grid = buildDom(scope, element, dataSource, settings, processCommand);
                    bmDataTableSvc.register(attrs.id, grid);
                    setUpToolTip();
                } else if (grid !== undefined && dataSource !== undefined) {
                    //clear the table
                    grid.clear();
                    //add data to table
                    grid.rows.add(dataSource).draw();
                    setUpToolTip();
                }
            });

            function setUpToolTip() {
                setTimeout(function () {
                    element.find('[data-toggle="tooltip"]').tooltip();
                }, 500);
            }

            function processCommand(args) {
                if (typeof callBackOnCommand == "function") {
                    common.safeApply(scope, function () {
                        callBackOnCommand({ result: args });
                    });
                }
            }

            bmDataTableSvc.onRowUpdated(attrs.id, onRowUpdated);

            function onRowUpdated(item) {
                var row = item.row;
                if (row !== undefined) {
                    if (item.commandName === "delete") {
                        row.remove().draw();
                    } else {
                        row.data(item.data);
                        row.invalidate('data').draw(false);
                    }
                } else {
                    throw "bmDataTableSvc Missing row object in the argument";
                }
            }

            scope.$on('$destroy', function cleanup() {
                if (grid != undefined && grid.destroy) {
                    grid.clear();
                    grid.destroy(true);
                }
                grid = undefined;
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .service('bmDataTableSvc', bmDataTableSvc)
       .provider('bmTableConfig', bmTableConfig);

    function bmTableConfig() {
        var self = this;
        self.config = {
            "data": [],
            "dom": 'lrtip',
            "bPaginate": false,
            "info": true,
            "aaSorting": [],
            "orderMulti": true, //Enable multi column sorting by holding shift key down
            "lengthChange": false,
            "deferRender": true,
            "displayLength": 10,
            "pagingType": "full_numbers",
            "language": {
                "emptyTable": "No items available to display.",
                "infoEmpty": "0 - 0 of 0 items",
                "info": "_START_ - _END_ of _TOTAL_ items",
            }
        };
        self.setConfig = function (appConfig) {
            self.config = $.extend(true, {}, self.config, appConfig);
        }
        this.$get = function () {
            return {
                config: this.config

            };
        }
    };

    bmDataTableSvc.$inject = ['$http'];

    function bmDataTableSvc($http) {
        var service = {
            register: register,
            unRegister: unRegister,
            getInstances: getInstances,
            onRowUpdated: onRowUpdated,
            update: update,
            adjustwidth: adjustwidth
        },
            instances = {},
            rowUpdateCallBacks = {};

        function register(id, dTable) {
            instances[id] = dTable;
        }

        function unRegister(id) {
            delete instances[id];
        }
        function adjustwidth(id) {
            var dTable = instances[id];
            if (dTable !== undefined) {
                setTimeout(function () {
                    dTable.columns.adjust().draw();
                }, 500);
            }
        }
        function getInstances() {
            var list = [];
            for (var key in instances) {
                if (instances.hasOwnProperty(key)) {
                    list.push(instances[key]);
                }
            }
            return list;
        }

        function onRowUpdated(id, callBack) {
            rowUpdateCallBacks[id] = callBack;
        }

        function update(id, args) {
            var callBack = rowUpdateCallBacks[id];
            if (typeof callBack == "function") {
                callBack(args);
            } else {
                throw "bmDataTableSvc No table created by id: " + id;
            }

        }

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmDatePopup', bmDatePopup);

    bmDatePopup.$inject = ['$compile'];

    function bmDatePopup($compile) {
        // Usage:
        //     <input data-bm-date-popup=""/>
        // Creates:
        // 
        var directive = {
            replace: false,
            terminal: true,
            priority: 1000,
            link: function link(scope, element, attrs) {
                element.attr('data-kendo-date-picker', '');
                if (!(element.attr('data-k-options') !== undefined || element.attr('data-k-options') !== undefined)) {
                    element.attr('data-k-options', "{ format: 'MM/dd/yyyy'}");
                }
                element.removeAttr("bm-date-popup"); //remove the attribute to avoid indefinite loop
                element.removeAttr("data-bm-date-popup"); //also remove the same attribute with data- prefix in case users specify data-common-things in the html

                $compile(element)(scope);
            }
        };
        return directive;

    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmDynamicSelect', bmDynamicSelect);

    bmDynamicSelect.$inject = ['commonSvc'];

    function bmDynamicSelect(common) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel'
        };

        return directive;
        function initiaLize(element, itemDataSource, selectedItem, onUserSelect) {
            var minimumInputLength = 3;
            if (_.size(itemDataSource) < 20) {
                minimumInputLength = 0;
            }

            element.select2({
                minimumInputLength: minimumInputLength,
                initSelection: function (elem, callback) {
                    if (selectedItem !== undefined && selectedItem.id) {
                        callback({ id: selectedItem.id, text: selectedItem.text });
                    }
                },
                query: function (query) {
                    var data = { results: [] };
                    _.each(itemDataSource, function (selectedItem) {
                        if (selectedItem.text.toLowerCase().indexOf(query.term.toLowerCase().trim()) > -1) {
                            data.results.push({ id: selectedItem.id, text: selectedItem.text });
                        }
                    }),
                     query.callback(data);
                },
                formatNoMatches: function (term) {
                    /* customize the no matches output */
                    return '<div class="input-group"><input class="form-control" id="newTerm" value="'
                    + term + '"><span class="input-group-btn"><a href="javascript:void(0);" ' +
                        'id="addNew" class="btn btn-primary">Create</a> </span>' +
                                '</div>';
                }
            }).parent().find('.select2-with-searchbox').on('click', '#addNew', function () {
                /* add the new term */
                var newTerm = toTitleCase($('#newTerm').val());
                selectedItem = { id: newTerm, text: newTerm };
                itemDataSource.push(selectedItem);
                $(element).select2('val', newTerm); // select the new term
                $(element).select2('close');		// close the dropdown
                onUserSelect(newTerm);
            });;
            function toTitleCase(str) {
                return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
            }

            element.on("select2-selecting", function (e) {
                var selectedUser = _.findWhere(itemDataSource, { id: e.val });
                if (selectedUser !== undefined) {
                    onUserSelect(selectedUser);
                }
            });
        }

        function destroy(element) {
            element.off('select2-selecting');
            element.select2("destroy");
        }

        function link(scope, element, attrs, ngModelCtrl) {
            var isInitialized = false,
                onSelectedUser = scope.$eval(attrs.onSelectedUser);
            if (element.select2) {

                scope.$watch(attrs.itemDataSource, function (itemDataSource) {
                    if (itemDataSource === undefined)
                        return;

                    if (isInitialized) {
                        destroy(element);
                        isInitialized = false;
                    }
                    if (_.size(itemDataSource)) {
                        var selectedItem = scope.$eval(attrs.ngModel);

                        initiaLize(element, itemDataSource, selectedItem, function (item) {
                            common.safeApply(scope, function () {
                                ngModelCtrl.$setViewValue(item);

                            });
                        });
                        isInitialized = true;
                    }
                });

                scope.$on('$destroy', function cleanup() {
                    destroy(element);
                });
            }
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmFileUpload', bmFileUpload);

    bmFileUpload.$inject = ['$compile'];

    function bmFileUpload($compile) {
        // Usage:
        //     <bmFileUpload></bmFileUpload>
        // Creates:
        // 
        var directive = {
            compile: compile,
            restrict: 'EA',
            replace: true,
            controller: ['$scope', '$element', controller],
            scope: {
                selectedFile: "=ngModel",
                onSelectedFile: "&",
                config: "="
            },
            template: '<div><div class="file-input-mask {{mergedConfig.btnClass}}">' +
                '{{mergedConfig.btnText}} <input type="file" ngf-select="" ng-model="selectedFile" ngf-change="validateFiles(selectedFile[0], $files)" >' +
                '<!-- DO NOT REMOVE THESE -->' +
                '<!--[if lte IE 9 ]><style>.js-fileapi-wrapper { position: static !important; }</style><![endif]-->' +
                '<!--[if lte IE 8 ]><style>.js-fileapi-wrapper input { filter: alpha(opacity=0); }</style><![endif]-->' +
                '</div>&nbsp;<span ng-if="mergedConfig.showFileName">{{fileName}}</span></div>'
        };
        var defaultConfig = {
            btnClass: 'btn btn-default',
            btnText: 'Browse',
            showFileName: true
        };
        function compile(tElement, tAttrs) {
            var destEl = tElement.find('input');
            angular.forEach(tAttrs.$attr, function (value, key) {
                if (key !== "ngModel" && key !== "bmFileUpload") {
                    value = tAttrs[key];
                    destEl.attr(key, value);
                }
            });

            var postLinkFn = function (scope, element, attrs) {
                // your link function
                // ...
            }

            return postLinkFn;
        }
        function link(scope, element, attrs) {


            // $compile(element)(scope);
        }

        function controller(scope, element) {
            scope.$evalAsync(function () {
                scope.mergedConfig = $.extend({}, defaultConfig, scope.config);
            });
            scope.fileName = "No files chosen";
            scope.validateFiles = function (selectedFile, $files) {
                if (selectedFile !== undefined && _.size(selectedFile) > 0) {
                    scope.fileName = selectedFile.name;
                    if (typeof scope.onSelectedFile == "function") {
                        scope.onSelectedFile({ file: selectedFile });
                    }
                } else {
                    scope.selectedFile = null;
                }
            }

        }

        return directive;

    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmIndicator', [
         '$window',
         '$timeout',
         bmIndicator
        ]);

    function bmIndicator($window, $timeout) {
        var opts = {
            lines: 13,
            length: 20,
            width: 10,
            radius: 30,
            corners: 1,
            rotate: 0,
            direction: 1,
            color: '#005892',
            speed: 1,
            trail: 60,
            shadow: false,
            hwaccel: false,
            className: 'spinner',
            zIndex: 2000000000,
            top: 'auto',
            left: 'auto'
        };

        function link(scope, element, attrs) {
            var spinOptions = scope.$eval(attrs.indicatorOptions) || {};
            var defaultOptions = angular.extend({}, opts);
            angular.extend(defaultOptions, spinOptions);
            var outer = element.css('padding');
            var div = $('<div style="position: absolute;top:0;left:0;width: 100%;height: 100%;z-index: 2000;">'
                    + '<div  style="background-color:white;height:100%"/>'
                    + '</div>');
            div.css('padding', outer);
            div.appendTo(element);
            div.hide();
            var indicator = $('<span style="position: absolute;top: 50%;left: 50%;"/>').appendTo(element);
            var spinner = new Spinner(defaultOptions).spin();
            indicator[0].appendChild(spinner.el);

            var throttledShow = _.debounce(setIndicatorVisibility, 100);
            scope.$watch(attrs.bmIndicator, function (showIndicator) {
                throttledShow(showIndicator, div, indicator);
            });
        }

        function setIndicatorVisibility(showIndicator, div, indicator) {
            if (showIndicator === true) {
                div.fadeIn(20, function () {
                    indicator.fadeIn();
                });
            } else {
                indicator.fadeOut(20, function () {
                    div.fadeOut();
                });
            }
        }

        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;
    }

})();
//https://github.com/sillsdev/web-ng-listview
angular.module('treehouse.ui')
  .directive('bmListView', ["$timeout", function ($timeout) {
      return {
          restrict: 'EA',
          transclude: true,
          replace: true,
          template: '<div class="listview" ng-hide="hideIfEmpty && items.length == 0"><div ng-transclude></div>' +
              '<div class="paginationblock">' +
              '<pagination boundary-links="true" total-items="itemsCount" items-per-page="itemsPerPage" ng-model="currentPage" ' +
              'previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" ' +
              'last-text="&raquo;">' +
              '</pagination>' +
              '<div ng-if="enableChangePageSize" class="pull-right pagination">Items per page: ' +
                  '<select ng-model="itemsPerPage">' +
                  '<option value="10" selected>10</option><option value="25">25</option><option value="50">50</option>' +
                  '<option value="100">100</option>' +
                  '</select>' +
              '</div></div></div>',
          scope: {
              search: "&",
              select: "&",
              items: "=",
              hideIfEmpty: "@",
              visibleItems: "=",
              pagingConfig: "="
          },
          controller: ["$scope", function ($scope) {
              var defaultPagingConfig = {
                  itemsPerPage: 10,
                  currentPage: 1,
                  enableChangePageSize: false
              },
                  mergedPagingConfig = $.extend({}, defaultPagingConfig, $scope.pagingConfig);

              $scope.itemsPerPage = mergedPagingConfig.itemsPerPage;  // This should match the default value for the selector above
              $scope.itemsCount = 0;
              $scope.currentPage = mergedPagingConfig.currentPage;
              $scope.enableChangePageSize = mergedPagingConfig.enableChangePageSize;

              this.activate = function (item) {
                  $scope.active = item;
                  $scope.select({
                      item: item
                  });
              };
              this.activateNextItem = function () {
                  var index = $scope.items.indexOf($scope.active);
                  this.activate($scope.items[(index + 1) % $scope.items.length]);
              };
              this.activatePreviousItem = function () {
                  var index = $scope.items.indexOf($scope.active);
                  this.activate($scope.items[index === 0 ? $scope.items.length - 1 : index - 1]);
              };
              this.isActive = function (item) {
                  return $scope.active === item;
              };
              this.selectActive = function () {
                  this.select($scope.active);
              };
              this.updateVisibleItems = function () {
                  var sliceStart;
                  var sliceEnd;
                  if ($scope.currentPage) {
                      sliceStart = ($scope.currentPage - 1) * $scope.itemsPerPage; // currentPage is 1-based
                      sliceEnd = $scope.currentPage * $scope.itemsPerPage;
                  } else {
                      // Default to page 1 if undefined
                      sliceStart = 0;
                      sliceEnd = $scope.itemsPerPage;
                  }
                  $scope.visibleItems = $scope.items.slice(sliceStart, sliceEnd);
              };
              this.updateItemsCount = function () {
                  $scope.itemsCount = $scope.items.length;
                  if ($scope.currentPage < 1) {
                      $scope.currentPage = 1;
                  }
              };
              this.query = function () {
                  $scope.search();
                  this.updateItemsCount();
                  //					$scope.search({
                  //						term : $scope.term
                  //					});
              };
          }],
          link: function (scope, element, attrs, controller) {
              scope.$watch('currentPage', function () {
                  controller.updateVisibleItems();
              });
              scope.$watch('itemsPerPage', function () {
                  controller.updateItemsCount();
                  controller.updateVisibleItems();
              });
              scope.$watch('items', function () {
                  controller.updateItemsCount();
                  controller.updateVisibleItems();
              }, true);
              controller.query();
          }
      };
  }])
;

(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmMultiDatePicker', bmMultiDatePicker);

    bmMultiDatePicker.$inject = ['commonSvc'];

    function bmMultiDatePicker(common) {
        // Usage:
        //     <bmMultiDatePicker></bmMultiDatePicker>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            scope: {
                disableBeforeThisDate: "=",
                eventDates: "=",
                selectedDates: "=ngModel"
            }
        };
        return directive;
        function configureDom(element, dates, disableBeforeThisDate, eventDates, defaultDate, onDateSelected, onChangeMonthYear) {
            var config = {
                onSelect: onDateSelected,
                beforeShowDay: beforeShowDay,
                defaultDate: defaultDate,
                onChangeMonthYear: onChangeMonthYear
            };

            if (dates != null && dates.length > 0) {
                config.addDates = dates;
            }


            element.multiDatesPicker(config);



            function beforeShowDay(dt) {
                dt = getFormattedDate(dt);
                var contract = [true, "", ""],
                     disDate = _.findWhere(eventDates, { eventDate: dt });

                if (disableBeforeThisDate !== null && dt <= disableBeforeThisDate) {
                    contract[0] = false;
                }

                if (disDate != null) {
                    if (disDate.isEnabled === false) {
                        contract[0] = false;
                    }
                    contract[1] = disDate.className || "";
                    contract[2] = disDate.toolTip || "";

                }
                return contract;
            }
        }

        function getFormattedDate(dt) {
            if (typeof dt == "string") {
                dt = new Date(dt);
            }

            if (typeof dt == "object") {
                //strip the time zone and convert date into integer format easier for comparison
                dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
            }
            return dt;
        }

        function getDates(dts) {
            var dates = [];
            _.each(dts, function (dt) {
                var date = getFormattedDate(dt);
                dates.push(date);
            });

            return dates;
        }

        function getEventDates(dts) {
            var dates = [];

            if (dts !== undefined) {
                _.each(dts, function (dt) {
                    dt.eventDate = getFormattedDate(dt.eventDate);
                    dates.push(dt);
                });
            }

            return dates;
        }

        function getDisabledDate(disableBeforeThisDate) {
            if (disableBeforeThisDate !== undefined) {
                return getFormattedDate(disableBeforeThisDate);
            }
            return null;
        }

        function destroy(element) {
            element.multiDatesPicker('resetDates', 'disabled');
            element.multiDatesPicker('resetDates', 'picked');
            element.multiDatesPicker('destroy');

        }
        function link(scope, element, attrs) {
            var isConfigured = false,
                eventDates = [],
                disableBeforeThisDate = null,
                isDateReset = false,
                    defaultDate = new Date();
            defaultDate = defaultDate.getDate();

            scope.$watch(function () {
                if (scope.selectedDates !== undefined)
                    return scope.selectedDates.length;
                return -1;
            }, function (count) {
                if (count === -1)
                    return;
                if (isDateReset === true) {
                    isDateReset = false;
                    return;
                }

                var selectedDates = scope.selectedDates;
                selectedDates = getDates(selectedDates);
                disableBeforeThisDate = getDisabledDate(scope.disableBeforeThisDate);
                eventDates = getEventDates(scope.eventDates);

                if (isConfigured) {
                    destroy(element);
                }

                configureDom(element, selectedDates, disableBeforeThisDate, eventDates, defaultDate, onDateSelected, onChangeMonthYear);
                isConfigured = true;
            });

            function onDateSelected() {
                isDateReset = true;
                common.safeApply(scope, function () {
                    var selectedDates = element.multiDatesPicker('getDates');
                    scope.selectedDates = selectedDates;
                });
            }

            function onChangeMonthYear(year, month) {
                defaultDate = new Date(year, month - 1, 1);
            }

            scope.$on('$destroy', function cleanup() {
                destroy(element);
            });

        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmProgressBar', bmProgressBar);

    bmProgressBar.$inject = ['$interval'];

    function bmProgressBar($interval) {
        // Usage:
        //     <bmProgressBar></bmProgressBar>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            template: '<div  data-ng-if="enableLoader" data-ng-animate="" class="page-cover dissolve-animation">' +
                '<div class="page-cover-background"></div><div class="page-cover-content">' +
                '<div class="row">' +
                '<div class="col-sm-8 col-sm-offset-2">' +
                '<div class="page-cover-message text-info">' +
                '{{loadingMessage}}' +
                '</div>' +
                '<div class="progress progress-striped active page-progress-bar">' +
                '<div class="progress-bar" role="progressbar"' +
                'aria-valuenow="{{percentageComplete}}"' +
                'aria-valuemin="0"' +
                'aria-valuemax="100"' +
                'data-ng-style="{ width: percentageComplete + \'%\' }">' +
                '</div>' +
                '<div class="text-info text-success text-bold text-left">' +
                '{{percentageComplete}}%' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></div></div>'

        };
        return directive;

        function link(scope, element, attrs) {
            element.css('position', 'relative');
            var stop;
            function startLoader() {
                scope.percentageComplete = 0;
                stop = $interval(function () {
                    var incrementer = Math.floor((Math.random() * 5) + 1);
                    scope.percentageComplete += incrementer;
                    if (scope.percentageComplete >= 100) {
                        scope.percentageComplete = 100;
                        $interval.cancel(stop);
                    }
                }, 1500);

                scope.$watch(attrs.percentageComplete, function (percentage) {
                    scope.loadingMessage = scope.$eval(attrs.loadingMessage);
                    if (percentage > scope.percentageComplete) {
                        scope.percentageComplete = percentage;
                    }
                });
            }

            scope.$watch(attrs.showOn, function (enableLoader) {
                scope.enableLoader = enableLoader;
                if (enableLoader === true) {
                    startLoader();
                }
                else if (enableLoader === false) {
                    $interval.cancel(stop);
                }
            });

        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmScrollInView', bmScrollInView);

    bmScrollInView.$inject = ['$window'];

    function bmScrollInView($window) {
        // Usage:
        //     <bmScrollInView></bmScrollInView>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA'

        };
        return directive;

        function link(scope, element, attrs) {
            scope.$watch(attrs.bmScrollInView, function (enable) {
                if (enable === true) {
                    var top = element.offset().top - 200;
                    $('html,body').animate({ scrollTop: top }, 500);
                }
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmSortable', bmSortable);

    bmSortable.$inject = ['$window'];

    function bmSortable($window) {
        // Usage:
        //     <bmSortable></bmSortable>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$evalAsync(function () {
                if (element.sortable !== undefined) {
                    element.sortable();
                }
            });
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.ui')
        .directive('bmSsnTextBox', bmSsnTextBox);

    bmSsnTextBox.$inject = ['commonSvc'];

    function bmSsnTextBox(commonSvc) {
        // Usage:
        //      <input data-bm-ssn-text-box="" data-ng-model=""/>
        // Creates:
        // 
        var directive = {
            link: link,
            require: 'ngModel',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var elem = $(element),
                originalValue = null,
                editableMask = "000-00-0000",
                maskedMask = "***-**-0000";

            elem.kendoMaskedTextBox({
                mask: maskedMask,
                clearPromptChar: true
            });
            var maskedInput = elem.data('kendoMaskedTextBox');

            ngModelCtrl.$formatters.push(function (num) {
                if (num !== null) {
                    originalValue = num;
                    hidSsn(elem);
                }
                return maskedInput.value();
            });

            elem.blur(function () {
                var $this = $(this);
                hidSsn($this);
            });

            function hidSsn($this) {
                if ($this.val().length > 0) {
                    originalValue = $this.val();
                }
                if (!_.isNullOrUndefined(originalValue)) {
                    var lastFour = originalValue.substring(originalValue.length - 4);
                    $this.val(lastFour);
                }
                maskedInput.setOptions({
                    mask: maskedMask
                });
            }

            elem.focus(function () {
                var $this = $(this);
                if (originalValue != null && originalValue.indexOf("*") == -1) {
                    $this.val(originalValue);
                    maskedInput.setOptions({
                        mask: editableMask
                    });
                } else {
                    elem.select();
                }
            });

            elem.on('change keyup paste mouseup input', function () {
                if (elem.val().match(/[\d]/) == null) {
                    maskedInput.setOptions({ mask: editableMask });
                }
            });


            scope.$on('$destroy', function cleanup() {
                if (maskedInput !== undefined && maskedInput.destroy) {
                    maskedInput.destroy();
                }
            });
        }
    }

})();
(function () {
    'use strict';

    var app = angular
        .module('treehouse.ui');

    app.filter('defaultDate', defaultDate);
    defaultDate.$inject = ['$filter'];
    function defaultDate($filter) {
        //Get reference to date filer.
        var angularDateFilter = $filter('date');
        return function (theDate) {
            return angularDateFilter(theDate, 'MM/dd/yyyy');
        };
    }

    app.filter('resolveUrl', resolveUrl);
    //resolveUrl.$inject = ['$filter'];
    function resolveUrl() {
        return function (url) {
            return treehouse.client.util.resolveUrl(url);
        };
    }

    app.filter('enumInfo', [
      'enumLookupSvc',
      function (enumLookupSvc) {
          return function (id, category, key) {
              key = key || 'Text';
              if (id != null) {
                  var enumType = enumLookupSvc.getEnumInfoByValue(category, id);
                  if (enumType != null) {
                      return enumType[key];
                  }
              }
              return '';
          };
      }
    ]);

})();
(function () {
    'use strict';

    angular.module('treehouse.util', ['treehouse.diagnostics', 'treehouse.form', 'treehouse.modal', 'treehouse.router']);
})();
(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .controller('ApplogCtrl', ApplogCtrl);

    ApplogCtrl.$inject = ['commonSvc', 'initialData', 'jsonFilter', 'dateFilter'];

    function ApplogCtrl(commongSvc, initialData, jsonFilter, dateFilter) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'ApplogCtrl';
        activate();
        // 
        function activate() {
            vm.logs = initialData.logs;
        }

        vm.dtConfig = getItemConfig();

        function getItemConfig() {
            var config = {
                "columns": [],
                "columnDefs": []
            }
            config.columns.push({ "title": "Id", "data": "id", "class": "text-right" });
            config.columns.push({ "title": "Log", "data": "message", "width": "10%" });
            config.columns.push({ "title": "Category", "data": "category" });
            config.columns.push({ "title": "Is Error", "data": "category" });
            config.columns.push({ "title": "Data", "data": "data" });
            config.columns.push({ "title": "Url", "data": "url" });
            config.columns.push({ "title": "Date", "data": "timeStamp" });
            config.columns.push({ "title": "Sent To Server", "data": "savedToServer" });


            config.columnDefs.push({
                "render": function (data, type, row) {
                    var control = '<pre style="width:300px;overflow-x:scroll;">' + jsonFilter(data) + '</pre>';
                    return control;
                },
                "targets": 4
            });

            config.columnDefs.push({
                "render": function (data, type, row) {
                    return dateFilter(data, 'HH:mm:ss.sss a');
                },
                "targets": 6
            });
            return config;
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .service('commonSvc', commonSvc)
        .provider('commonConfig', commonConfig);

    function commonConfig() {
        this.config = {
            events: {
                spinnerToggleEvent: 'spinnerToggleEvent'
            }
            // These are the properties we need to set
            //controllerActivateSuccessEvent: '',
            //spinnerToggleEvent: ''
        };
        this.$get = function () {
            return {
                config: this.config
            };
        }
    }

    commonSvc.$inject = ['$q', '$rootScope', 'commonConfig', 'loggerSvc', 'validationHelperSvc', 'bmModalSvc', 'errorHandlerSvc', 'enumLookupSvc'];

    function commonSvc($q, $rootScope, commonConfigProvider, loggerSvc, validation, bmModalSvc, errorHandlerSvc, enumLookupSvc) {
        var dialogue = {
            confirm: bmModalSvc.confirm
        },
            loader = {
                show: function () {
                    broadcast(commonConfigProvider.config.events.spinnerToggleEvent, true);
                },
                hide: function () {
                    broadcast(commonConfigProvider.config.events.spinnerToggleEvent, false);
                }
            },
            service = {
                activateController: activateController,
                loader: loader,
                logger: loggerSvc,
                validation: validation,
                copy: copy,
                safeApply: safeApply,
                broadcast: broadcast,
                dialogue: dialogue,
                errorHandler: errorHandlerSvc,
                enumLookup: enumLookupSvc,
                util: treehouse.client.util
            };


        function activateController(promises, controllerId) {
            var data = { controllerId: controllerId };
            return $q.all(promises).then(function (eventArgs) {
                //$broadcast(commonConfig.config.controllerActivateSuccessEvent, data);
            }, function (error) {
                logger.logError("Error occurred: " + error.data.messageDetail, error, controllerId, true);
                data.error = error;
                // $broadcast(commonConfig.config.controllerActivateFail, data);
            });
        }

        function broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function copy(source, deep) {
            deep = deep || true;
            return jQuery.extend(deep, {}, source);
        }

        function safeApply(scope, fn) {
            var phase = scope.$root != null ? scope.$root.$$phase : "unknown";
            if (phase == '$apply' || phase == '$digest' || phase == 'unknown') {
                if (fn && (typeof (fn) === 'function')) {
                    fn();
                }
            } else {
                scope.$apply(fn);
            }
        }

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .factory('enumLookupSvc', enumLookupSvc);

    //enumLookupSvc.$inject = ['loggerSvc', '$q'];

    function enumLookupSvc() {
        var lookup = treehouse.enumLookup || {};
        return lookup;
    }
})();
(function () {
    'use strict';
    angular
        .module('treehouse.util').config([
        '$provide',
        function ($provide) {
            $provide.decorator('$exceptionHandler', [
                '$delegate',
                'loggerSvc',
                '$window',
                extendExceptionHandler
            ]);
        }
        ]);

    // Extend the $exceptionHandler service to also display a toast.
    function extendExceptionHandler($delegate, logger, $window) {
        var log = logger.get('exception.handler');
        return function (exception, cause) {
            $delegate(exception, cause);

            try {
                var errorData = {
                    errorUrl: $window.location.href,
                    exception: exception,
                    cause: cause
                };
                //stackTrace: printStackTrace({ e: exception })

                var msg = exception.message;
                log.error(msg, errorData, true);
            } catch (ex) {
                log.error('Error occurred while trying to get error from exception handler', ex, true);
            }
        };
    }

})();
(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .controller('LayoutCtrl', LayoutCtrl);

    LayoutCtrl.$inject = ['$rootScope', 'commonSvc', 'commonConfig'];

    function LayoutCtrl($rootScope, common, commonConfigProvider) {
        /* jshint validthis:true */
        var vm = this,
            logger = common.logger.get('LayoutCtrl');
        vm.title = 'Layout';
        vm.showSplash = false;
        vm.isBusy = true;
        vm.spinConfig = {
            lines: 13,
            length: 20,
            width: 10,
            radius: 30,
            corners: 1,
            rotate: 0,
            direction: 1,
            color: '#005892',
            speed: 1,
            trail: 60,
            shadow: false,
            hwaccel: false,
            className: 'spinner',
            zIndex: 2000000000,
            top: 'auto',
            left: 'auto'
        };

        activate();

        function activate() {
            logger.success('Angular loaded!', null, true);
        }

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                vm.isBusy = false;
            });

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                vm.isBusy = true;
            });

        $rootScope.$on(commonConfigProvider.config.events.spinnerToggleEvent,
            function (event, data) {
                vm.isBusy = data;
            });
    }
})();

(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .controller('OopsCtrl', OopsCtrl);

    OopsCtrl.$inject = ['commonSvc', 'initialData'];

    function OopsCtrl(common, initialData) {
        /* jshint validthis:true */
        var vm = this,
            logger = common.logger.get('OopsCtrl');
        vm.title = 'OopsCtrl';

        activate();

        function activate() {
            logger.warn("error logs", initialData, true);
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('treehouse.util')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [
            {
                state: "oops",
                config: {
                    url: "/oops",
                    controller: "OopsCtrl as oops",
                    template: '<div>' +
                        '<h4 class="page-header">Error Occurred</h4>' +
                            '<div class="row">' +
                                '<div class="col-sm-8 col-sm-offset-2">' +
                                    '<div class="alert alert-danger">' +
                                        '<span>' +
                                        'An unknown error occurred.' +
                                        '</span>' +
                                        '<br />' +
                                        '<br />' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                          '<hr />' +
                        '</div>',
                    resolve: {
                        initialData: ['commonSvc', function (commonSvc) {
                            return commonSvc.logger.getLogs();
                        }]
                    },
                    data: {
                        title: "Error"
                    }
                }
            },
            {
                state: "404",
                config: {
                    url: "/404",
                    template: '<div>' +
                        '<h4 class="page-header">Page Not Found</h4>' +
                            '<div class="row">' +
                                '<div class="col-sm-8 col-sm-offset-2">' +
                                    '<div class="alert alert-danger">' +
                                        '<span>' +
                                        'We could not locate the page. Please click on the home button to go back.' +
                                        '</span>' +
                                        '<br />' +
                                        '<br />' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                          '<hr />' +
                        '</div>',
                    data: {
                        title: "Page Not Found"
                    }
                }
            }, {
                state: "applog",
                config: {
                    url: "/applog",
                    controller: "ApplogCtrl as errorLog",
                    template: ' <div>' +
                        '<h4> Total Count: {{errorLog.logs.length}}</h4>' +
                        '<table id="appLog" data-bm-data-table=""  page-size="200"' +
                          'data-item-datasource="errorLog.logs" data-config="errorLog.dtConfig"' +
                          'class="table table-striped table-bordered">' +
                        '</table>' +
                       '</div>',
                    resolve: {
                        initialData: ['commonSvc', function (commonSvc) {
                            return commonSvc.logger.getLogs();
                        }]
                    }
                },
                data: {
                    title: "App log"
                }
            }
        ];
    }
})();