(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .controller('TrialBalanceCtrl', TrialBalanceCtrl);

    TrialBalanceCtrl.$inject = ['$scope','$q','commonSvc', '$rootScope','repoBaseSvc','contextSvc', '$http'];

    function TrialBalanceCtrl($scope,$q,common,$rootScope, repo, contextSvc, $http) {
        /* jshint validthis:true */
        var vm = this;
        $rootScope.htmlClass = '';
        var showToaster = true;
        var dealRepo = repo.service('Deal');
        var companyRepo = repo.service('company');
        var investmentRepo = repo.service('investment');
        vm.title = 'K1 Entry';
        vm.table = [];

        vm.q1Total = 0;
        vm.q2Total = 0;
        vm.q3Total = 0;
        vm.q4Total = 0;
        vm.yearTotal = 0;
        vm.ytdTotal = 0;
        vm.accountSetTotal = 0;
        vm.showInvestmentData = false;

        activate();

        function activate() {
            var context = contextSvc.getCurrentContext();
            var orgId = context.organization.organizationId;
            var workgroupId = context.company.workGroupId;
            dealRepo.getList({"organizationId": orgId}).then(function (result) {
                return vm.deals = result;
            })

            companyRepo.getList({"workgroupId": workgroupId}).then(function (result) {
                return vm.companies = result;
            })

            vm.loadInvestment = function () {
                vm.showInvestmentData = false;
                var defer = $q.defer();
                var orgId = context.organization.organizationId;
                var sessionId = context.session.sessionId;
                var companyId = context.company.companyId;
                var divId = vm.selectedInvestmentEntity.companyId;
                var dealId = vm.selectedDeal.dealId;
                /*investmentRepo.getList({
                    "organizationId": orgId,
                    "sessionId": sessionId,
                    "companyId": companyId,
                    "divId": divId,
                    "dealId": dealId
                })*/
                $http.get('http://172.24.17.160/TreeHouseDevApi/investment?companyId=675&dealId=603&divId=675&organizationId=10&sessionId=77')
                    .then(function (result) {

                        if(!result){
                            vm.accounts = {};
                            vm.showInvestmentData = false;
                            return;
                        }

                        vm.accounts = result.accountSets;
                        calculateTotal();
                        enableWatch();
                        defer.resolve(vm.accounts);
                        vm.showInvestmentData = true;

                    }, function (result) {
                        defer.reject(result);
                    });

                return defer.promise;
            }
        }


        function calculateTotal(){
            resetTotals();
            var total = 0;
            $.each(vm.accounts,function(index,elem){

                $.each(elem.accounts,function(childIndex, childElem){
                    childElem.rowTotal = (parseInt(childElem.q1) || 0) + (parseInt(childElem.q2) || 0)  + (parseInt(childElem.q3) || 0)  + (parseInt(childElem.q4) || 0);
                    vm.yearTotal += childElem.rowTotal;
                    vm.q1Total += (parseInt(childElem.q1) || 0);
                    vm.q2Total += (parseInt(childElem.q2) || 0);
                    vm.q3Total += (parseInt(childElem.q3) || 0);
                    vm.q4Total += (parseInt(childElem.q4) || 0);
                    childElem.ytdTotal = childElem.rowTotal;
                    total += childElem.rowTotal;
                    vm.ytdTotal += childElem.ytdTotal;
                });

                elem.total = total;
            });
            //enableWatch();
        }

        function resetTotals(){
            vm.q1Total = 0;
            vm.q2Total = 0;
            vm.q3Total = 0;
            vm.q4Total = 0;
            vm.yearTotal = 0;
            vm.ytdTotal = 0;
        }


        vm.cancel = function(){
            vm.accounts = undefined;
        }


        function enableWatch(){
            vm.accountsWatch =  $scope.$watch('vm.accounts',function(accountVal){
                if (vm.accounts)
                    calculateTotal();
            },true);
        }


        $(document).on('click', 'a[data-toggle-row-group]', function(){
            var group = $(this).data('toggle-row-group');

            if($(this).hasClass('closed')){
                $(this).removeClass('closed').addClass('opened');
                $('tr[data-row-group="' + group + '"]').removeClass('hidden');
                $(this).find('i.fa').removeClass('fa-plus-square').addClass('fa-minus-square-o');
            }else{
                $(this).removeClass('opened').addClass('closed');
                $(this).find('i.fa').removeClass('fa-minus-square-o').addClass('fa-plus-square');
                $('tr[data-row-group="' + group + '"]').addClass('hidden');
            }

        })

    }
})();
