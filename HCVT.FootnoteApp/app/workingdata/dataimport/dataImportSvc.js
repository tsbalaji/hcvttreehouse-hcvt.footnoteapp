(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .factory('dataImportSvc', dataImportSvc);

    dataImportSvc.$inject = ['commonSvc', 'repoBaseSvc'];

    function dataImportSvc(common, repo) {

        var importRepo = repo.service('DataLoad');

        var service = {
            import: importDataLoad,

        };

        return service;

        function importDataLoad(file,vm) {
            return importRepo.saveWithFile(file, vm);
        }




        return service;
    }
})();