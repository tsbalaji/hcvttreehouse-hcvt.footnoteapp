(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .controller('DataImportCtrl', DataImportCtrl);

    DataImportCtrl.$inject = ['$scope','$sessionStorage','commonSvc','dataImportSvc','validationHelperSvc'];

    function DataImportCtrl($scope, $sessionStorage,common, dataImportSvc, validation) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        vm.title = 'Standard Footnotes';
        vm.workSheets = [];
        vm.model = {};
        activate();

        function activate() {
            return true;
        }

        vm.areWorkSheetsLoaded = function() {
            return vm.workSheets.length > 0;
        }

        vm.loadWorkSheetNames = function(t) {
            var reader = new FileReader();
            var file = vm.fileToUpload;

            reader.onload = function(e) {
                var data = e.target.result;
                var wb = XLSX.read(data, { type: 'binary' });
                $scope.$apply(function() {
                    vm.workSheets = wb.SheetNames;
                    vm.model.workSheet = vm.workSheets[0];
                });

            }

            reader.readAsBinaryString(file);

        }

        vm.save = function () {
            vm.showError = false;
            var context = $sessionStorage.userContext;
            var userContext = {
                organizationId: context.organization.organizationId,
                sessionId: context.session.sessionId,
                workgroupId: context.workgroup != undefined ? context.workgroup.workgGroupId : -1,
                companyId: context.company.companyId
            }
            common.loader.show();
            validation.removeAll(vm.validationGroupId);
            dataImportSvc.import(vm.fileToUpload, $.extend(userContext,vm.model)).then(function (result) {
                toastr.success("File Imported Successfully");
            }).catch(common.errorHandler.catch(vm.validationGroupId))
            .finally(function () {
                    common.loader.hide();
                });
        }

    }

})();
