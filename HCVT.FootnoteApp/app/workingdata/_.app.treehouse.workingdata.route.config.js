﻿(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .run(routeConfig);

    routeConfig.$inject = ['routeHelper', 'resolveUrlFilter'];

    function routeConfig(routeHelper, resolveUrl) {
        routeHelper.configureRoutes(getRoutes(resolveUrl));
    }

    function getRoutes(resolveUrl) {
        return [

            {
                state: 'root.workingdatahome',
                config: {
                    url: '^/blank',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdatahome.html'),
                            controller: 'WorkingDataHomeCtrl as vm'
                        }
                    }
                }
            },
            {
                state: 'root.k1-entry',
                config: {
                    url: '^/investments',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdata/investments/k1-entry.html'),
                            controller: 'K1EntryCtrl as vm',

                        }
                    }
                }
            },
            {
                state: 'root.standard-footnotes',
                config: {
                    url: '^/standard-footnotes',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdata/footnotes/standard-footnotes.html'),
                            controller: 'StandardFootnotesCtrl as vm'
                        }
                    }
                }
            },
            {
                state: 'root.allocation-percentages',
                config: {
                    url: '^/allocation-percentages',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdata/setup/allocation/allocation-percentages.html'),
                            controller: 'AllocationPercentagesCtrl as vm'
                        }
                    }
                }
            },
            {
                state: 'root.data-import',
                config: {
                    url: '^/data-imports',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdata/dataimport/data-import.html'),
                            controller: 'DataImportCtrl as vm'
                        }
                    }
                }
            },
            {
                state: 'root.trial-balance',
                config: {
                    url: '^/trial-balance',
                    views: {
                        'main@root': {
                            templateUrl: resolveUrl('app/workingdata/trialbalance/trial-balance.html'),
                            controller: 'TrialBalanceCtrl as vm'
                        }
                    }
                }
            }
        ];
    }
})();