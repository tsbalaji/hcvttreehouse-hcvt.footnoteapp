(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .controller('AllocationPercentagesCtrl', AllocationPercentagesCtrl);

    AllocationPercentagesCtrl.$inject = ['$scope', 'commonSvc', 'authSvc', '$rootScope'];

    function AllocationPercentagesCtrl($scope, common, authSvc, $rootScope) {
        /* jshint validthis:true */
        var vm = this;
        $rootScope.htmlClass = '';
        var showToaster = true;
        vm.title = 'Allocation Percentages';

        activate();

        function activate() {

            vm.table = {
                totals: {
                    'beginning': 1,
                    'Q1_change': 0,
                    'Q1_total': 1,
                    'Q2_change': 0,
                    'Q2_total': 1,
                    'Q3_change': 0,
                    'Q3_total': 1,
                    'Q4_change': 0,
                    'Q4_total': 1,
                },
                partners: [
                    {
                        'code': 'P1-ABC',
                        'beginning': .5,
                        'Q1_change': -.5,
                        'Q1_total': 0,
                        'Q2_change': 0,
                        'Q2_total': 0,
                        'Q3_change': 0,
                        'Q3_total': 0,
                        'Q4_change': 0,
                        'Q4_total': 0,
                        'active': false
                    },
                    {
                        'code': 'P2-DEF',
                        'beginning': 10,
                        'Q1_change': -.5,
                        'Q1_total': 0,
                        'Q2_change': 0,
                        'Q2_total': 0,
                        'Q3_change': 0,
                        'Q3_total': 0,
                        'Q4_change': 0,
                        'Q4_total': 0,
                        'active': true
                    },
                    {
                        'code': 'P3-FHI',
                        'beginning': 10,
                        'Q1_change': -.5,
                        'Q1_total': 0,
                        'Q2_change': 0,
                        'Q2_total': 0,
                        'Q3_change': 0,
                        'Q3_total': 0,
                        'Q4_change': 0,
                        'Q4_total': 0,
                        'active': false
                    },
                    {
                        'code': 'P3-JKL',
                        'beginning': 10,
                        'Q1_change': -.5,
                        'Q1_total': 0,
                        'Q2_change': 0,
                        'Q2_total': 0,
                        'Q3_change': 0,
                        'Q3_total': 0,
                        'Q4_change': 0,
                        'Q4_total': 0,
                        'active': true
                    }
                ]
            }

        }

    }

    angular.module('treehouse.workingdata')
        .directive('inputPercentage', function(){
            return {
                restrict: 'AE',
                require: '^ngModel',
                scope: {
                    ngModel: '='
                },
                template: '<div class="input-group-feedback pull-right">' +
                                '<input type="text" class="text-entry form-control size-sm" ng-model="ngModel"/>' +
                                '<span class="form-control-feedback">%</span>' +
                            '</div>',
                replace: true
            }
        })

})();
