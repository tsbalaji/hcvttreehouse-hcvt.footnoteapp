(function () {
    'use strict';

    angular
        .module('treehouse.workingdata')
        .controller('StandardFootnotesCtrl', StandardFootnotesCtrl);

    StandardFootnotesCtrl.$inject = ['$scope', 'commonSvc', 'authSvc', '$rootScope', '$http'];
    var templates = {"free-text":'app/',"account":"",question:""}

    function StandardFootnotesCtrl($scope,common,authSvc,$rootScope,$http) {
        /* jshint validthis:true */
        var vm = this;
        var showToaster = true;
        vm.title = 'Standard Footnotes';
        //This there values should be passed while calling the footnote application in order to get the footnotes
        var orgid = 7;
        var sessionid= 68;
        var code = 200;

        vm.sectionModelData = [];

        activateDummyData();
        // activate();
        populateFundDetails();
        populateFundInternalInvestmentsDetails();
        populateFundExternalInvestmentsDetails();
       // populateFootnotes();
        populateDeals();
        populateInvestmentEntity();
      //  populateSections();

        function activate() {
            return true;
        }

        function activateDummyData() {
            vm.range = function (n) { return new Array(n); }

            vm.footnoteMaxChars = 50;

            vm.footnotes = [
                { 'name': 'footnote-1', 'leadingSpaces': 0, 'text': '' },
                { 'name': 'footnote-2', 'leadingSpaces': 0, 'text': '' },
                { 'name': 'footnote-3', 'leadingSpaces': 0, 'text': '' },
            ];

            vm.qa = {
                'text': {
                    'maxChars': 50
                }
            }

        }
        vm.sectionData = {
            "FreeText": {
                Collection: {}
            },
            "QA": {
                Collection: {}
            },
            "Account": {
                Collection: {}
            }
        }
        vm.addFootnote = function (sectionModelData, sec_id) {
            return sectionModelData.push({ 'section_id': sec_id, 'free_text_id': '', 'free_text': '', 'IsBold': false, 'IsItalics': false, 'IsUnderline': false, 'charCount': 0 })
        }

        vm.removeFootnote = function (sectionModelData,index) {
            return sectionModelData.splice(index, 1);
        }
        //Populate Footnotes
        // item.footnoteid will be present only when called from edit function
        vm.populateFootnotes = function (item) {
            //Footnotes dropdown
            vm.arrFootnotes = [];
            vm.sectionModelData = [];
            $http.get("/api/Footnote/", { params: { "org_id": orgid, "session_id": sessionid, "code": code } }).success(function (data) {
                vm.arrFootnotes = (data);
                if (item !=null && item.footnoteid) {
                    //to show the appropriate value in the drop down for footnote, deal and vehicle
                    vm.selectedFootnote = vm.arrFootnotes.filter(function (fnitem) { return fnitem["footnoteid"] == item.footnoteid.toString() })[0];
                    vm.selectedDeals = vm.arrDeals.filter(function (deal) { return deal["DealID"] == item.dealid.toString() })[0];
                    vm.selectedVehicles = vm.arrVehicles.filter(function (vehicle) { return vehicle["CompanyID"] == item.vehicleid.toString() })[0];
                    vm.setSelectedFootnoteId(vm.selectedFootnote,true)
                }
                else {
                   //Create new footnote
                }
            }).error(function (status) {
                toastr.error(status);
            });
        }
        //Populate Fund Details
        function populateFundDetails() {
            //Fund details panel
            vm.arrFundDetails = [];
            $http.get("/api/FundDetails/").success(function (data) {
                vm.arrFundDetails = (data);
            }).error(function (status) {
                toastr.error(status);
            });
        }
        //Populate Fund Internal Investments Details
        function populateFundInternalInvestmentsDetails() {
            //Fund Internal Investments Panel
            vm.arrFundInternalInvestments = [];
            $http.get("/api/FundInternalInvestments/").success(function (data) {
                vm.arrFundInternalInvestments = (data);
            }).error(function (status) {
                toastr.error(status);
            });
        }
        //Populate Fund External Investments Details
        function populateFundExternalInvestmentsDetails() {
            //Fund External Investments Panel
            vm.arrFundExternalInvestments = [];
            $http.get("/api/FundExternalInvestments/").success(function (data) {
                vm.arrFundExternalInvestments = (data);
            }).error(function (status) {
                toastr.error(status);
            });
        }
        
        vm.setSelectedFootnoteId = function (item, isFromEdit) {
            //called when footnote dropdown changed
            //set the values for deals and vehicle dropdown
            vm.selectedDeals = vm.arrDeals.filter(function (deal) { return deal["DealID"] == item.dealid.toString() })[0];
            vm.selectedVehicles = vm.arrVehicles.filter(function (vehicle) { return vehicle["CompanyID"] == item.vehicleid.toString() })[0];
            //Populate the sections tabs
            populateSections(item.footnoteid,isFromEdit);
            vm.shouldShowTabs = true;
        }
        //Populate Deals
        function populateDeals() {
            //Deals dropdown
            vm.arrDeals = [];
            $http.get("/api/Deals/" + orgid).success(function (data) {
                vm.arrDeals = data;
            }).error(function (status) {
                toastr.error(status);
            });
        }
        //Populate Investment Entity
        function populateInvestmentEntity() {
            //Investment Entity dropdown
            vm.arrVehicles = [];
            $http.get("/api/InvestmentVehicle/" + orgid).success(function (data) {
                vm.arrVehicles = data;
            }).error(function (status) {
                toastr.error(status);
            });
        }
        //Section(Tabs) Details
        function populateSections(selectedFootnoteId, isFromEdit) {
            //Section (Tabs) details
            vm.arrSections = [];
            $http.get("/api/Section/" + selectedFootnoteId).success(function (data) {
                vm.arrSections = data;
                if (isFromEdit) {
                    vm.editTabControls(data[0]["sections_id"], data[0]["section_type_id"])
                }
            }).error(function (status) {
                toastr.error(status);
            });
        }
       
        vm.constructTabControls = function (sectypid, sectionid) {
            if (sectypid == '1') {
                sectypid = "FreeText";
            } else if (sectypid == '2') {
                sectypid = "QA";
            } else if (sectypid == '3') {
                sectypid = "Account";
            }
            vm.selectedTabId = sectypid;
            vm.sectionId = sectionid;
            //Push the section model data to section data array
            vm.sectionData[sectypid]['Collection'][sectionid] = [];
            vm.sectionModelData = vm.sectionData[sectypid]['Collection'][sectionid];
        }
        //Construct tab html controls
        vm.editTabControls = function (sectypid, sectionid) {
            if (sectypid == '1') {
                sectypid = "FreeText";
            } else if (sectypid == '2') {
                sectypid = "QA";
            } else if (sectypid == '3') {
                sectypid = "Account";
            }
            vm.selectedTabId = sectypid;
            vm.sectionId = sectionid;
            vm.changeBold = function (footnote) {
                footnote.IsBold = !footnote.IsBold;
            }
            vm.changeItalic = function (footnote) {
                footnote.IsItalics = !footnote.IsItalics;
            }
            vm.changeUnderline = function (footnote) {
                footnote.IsUnderline = !footnote.IsUnderline;
            }
            vm.arrFreetextTab = [];
            switch (sectypid) {
                case 'FreeText':
                    // Free text
                    //,{params:{section_Id: sectionId}}
                    $http.get("/api/FreeText/" + sectionid).success(function (data) {
                        //vm.arrFreetextTab = data;\
                        if (!vm.sectionData[sectypid]['Collection'][sectionid]) {
                            vm.sectionData[sectypid]['Collection'][sectionid] = data;
                        }
                        vm.sectionModelData = vm.sectionData[sectypid]['Collection'][sectionid];
                        
                    }).error(function (status) {
                        toastr.error(status);
                    });
                   
                    break;
                case 'QA':
                    // Question and Answer
                    $http.get("/api/QuestionAndAnswer/" + sectionid).success(function (data) {
                        
                        if (!vm.sectionData[sectypid]['Collection'][sectionid]) {
                            vm.sectionData[sectypid]['Collection'][sectionid] = data;
                        }
                        vm.sectionModelData = vm.sectionData[sectypid]['Collection'][sectionid];

                    }).error(function (status) {
                        toastr.error(status);
                    });
                    break;
                case 'Account':
                    // Account
                    //if (!vm.sectionData[id][sectionId]) {
                    //    vm.sectionData[id][sectionId] = [];
                    //}
                    //vm.sectionModelData = vm.sectionData[id][sectionId];
                    break;
            }
           
        }

        vm.save = function () {
        //    alert(JSON.stringify(vm.sectionData).toString());
           $http({
                    method: 'POST',
                    url: 'api/saveFootnote',
                    data: JSON.stringify(vm.sectionData),
                    contentType: "application/json"
                })
            .success(function (response) {
                toastr.success('successfully saved. ' + response);
            })
            .error(function (error) {
                toastr.error('error:' + error);
            });
        }
        vm.isEdit = false;
        vm.edit = function (item) {
            vm.isEdit = true;
         
            vm.shouldShowDropdowns = true;
            //send the whole array to get deal and vehicle ids
            vm.populateFootnotes(item);
        }
        vm.cancel = function () {
            //Clear sectionData,sectionModelData
          //  vm.sectionData = [];
            //  vm.sectionModelData = [];
            //Reset footnote/Deals/Investment vehicle dropdowns
            vm.selectedFootnote = "";
            vm.selectedDeals = "";
            vm.selectedVehicles = "";
            //Hide Footnote/Deals/Investment vehicle dropdowns
            vm.shouldShowDropdowns = false;
            //Hide Section tabs
            vm.shouldShowTabs = false

        }

        vm.initializeTabControls = function (id, sectionId) {
            if (vm.isEdit) {
                vm.editTabControls(id, sectionId);
            } else {
                vm.constructTabControls(id, sectionId);
            }
        }
    }

})();
