$(function(){

    $.fn.rowLinker = function(){
        return this.find('tr[data-row-link]').on('click', function(){
            window.location.href = $(this).data('row-link');
        })
    }

    $.fn.panelCollapse = function(){

        var panel = this;

        panel.children('.panel-body').hide();
        panel.children('.panel-heading')
            .append('<div class="panel-heading-actions"><a href="#" class="collapse-card" data-state="closed"><i class="fa fa-fw fa-chevron-up"></i></a></div>')

        $('.collapse-card').on('click', function(e){
            e.preventDefault();

            currentCard = $(this).closest('.panel-card');

            // If it's closed
            if($(this).data('state') == 'closed'){
                currentCard.find('.panel-body').slideDown('fast');
                $(this).find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                return $(this).data('state', 'open');
            }

            currentCard.find('.panel-body').slideUp('fast');
            $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            return $(this).data('state', 'closed');

        });

        return this;
    }
})
