﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace HCVT.FootnoteApp.Service
{
    public class SaveFootnoteService
    {
        public string PutFootnoteDetails(SaveFootnoteDetailsModel.RootObject SaveFootnoteDetailsValues)
        {
            string sqlText="";
            int footnoteinserted = 0;
            List<SaveFootnoteDetailsModel.FreeTextRowItem> freeTextList = new List<SaveFootnoteDetailsModel.FreeTextRowItem>();
            foreach (IList<SaveFootnoteDetailsModel.FreeTextRowItem> v in SaveFootnoteDetailsValues.FreeText.Collection.Values)
                freeTextList.AddRange(v);
            foreach (var freetext in freeTextList)
            {   //Insert new footnote
                if (freetext.free_text_id == "")
                {
                    sqlText += "INSERT INTO [wd].[wd_fn_section_free_text_line] VALUES(7,68,641,13,1,'" + freetext.section_id + "',4,'" + freetext.free_text + "',0,'" + freetext.IsBold + "','" + freetext.IsItalics + "','" + freetext.IsUnderline +"');";
                }
                else //Update existing footnote
                {
                    sqlText += "UPDATE [wd].[wd_fn_section_free_text_line]"
                               + " SET [org_id] = '7'" 
                               +" ,[session_id] = '68'" 
                               +" ,[cmpcode_id] = '641'" 
                               +" ,[footnote_id] = '13'" 
                               +" ,[header_id] = '1'" 
                               +" ,[section_id] = '" + freetext.section_id + "'" 
                               +" ,[sort_order] = '2'"
                               + " ,[free_text] = '" + freetext.free_text + "'" 
                               +" ,[indent_number] = '0'" 
                               +" ,[is_bold] = '" + freetext.IsBold + "'"
                               + " ,[is_italic] = '" + freetext.IsItalics + "'"
                               + " ,[is_underline] = '" + freetext.IsUnderline + "'" 
                               +"  WHERE id= '" + freetext.free_text_id + "'";
                }
                
            }
            if (sqlText != "")
            { 
                using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
                {

                    using (SqlCommand objComm = new SqlCommand())
                    {

                        // objComm.CommandType = CommandType.StoredProcedure;
                        objComm.CommandType = CommandType.Text;
                        objComm.CommandText = sqlText;
                        objComm.Connection = myConn;
                        myConn.Open();
                        //"[dbo].[usp_FootnoteInsertOrUpdate]";
                       footnoteinserted = objComm.ExecuteNonQuery();

                    } //Command using end
                } //Conn using end
                return footnoteinserted + " Footnotes";
            }
            return 0 + " Footnotes";
        } //Method end
    } // Class end
} //Namespace end