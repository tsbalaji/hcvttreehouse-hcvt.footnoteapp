﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;

namespace HCVT.FootnoteApp.Service
{
    public class QuestionAndAnswerDetailsService
    {
        public List<QuestionAndAnswerDetailsModel> GetQuestionAndAnswerDetails(string secID)
        {
            List<QuestionAndAnswerDetailsModel> QuestionAndAnswerDetails = new List<QuestionAndAnswerDetailsModel>();
            string qryFootnotes = "select qal.id,qal.question_rpt,qal.question_gui_label,qal.answer_data_type,adt.description,qal.is_required,qal.is_lookup,ak.id,ak.code,ak.short_name" +
                                  " from Wd.fn_section_qa_list qal" +
                                  " left join wd.wd_fn_section_qa_line sqa on qal.id = sqa.qa_list_id" +
                                  " left join Wd.Fn_answer_key ak on ak.id = qal.answer_key_id" +
                                  " left join Wd.fn_answer_data_type adt on adt.id = qal.answer_data_type" +
                                  " --left join Wd.fn_answer_key_list akl on akl.answer_key_id = ak.id" +
                                  " left join  wd.wd_fn_section s on s.id = sqa.section_id --and s.footnote_id = sft.footnote_id" +
                                  " where s.footnote_id = 13 and sqa.section_id = '" + secID +
                                  " order by sqa.sort_order";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                myConn.Open();
                using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                {
                   
                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                QuestionAndAnswerDetails.Add(new QuestionAndAnswerDetailsModel {
                                    question_rpt = reader["question_rpt"].ToString(),
                                    question_gui_label = reader["question_gui_label"].ToString(),
                                    answer_data_type = reader["answer_data_type"].ToString(),
                                    description = reader["description"].ToString(),
                                    short_name = reader["short_name"].ToString()
                                });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return QuestionAndAnswerDetails;
                } //Command using end
            } //Conn using end
        } //Method end
    } // Class end
} // Namespace end