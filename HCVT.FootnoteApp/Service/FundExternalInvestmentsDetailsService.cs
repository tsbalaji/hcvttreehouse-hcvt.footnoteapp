﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;
namespace HCVT.FootnoteApp.Service
{
    public class FundExternalInvestmentsDetailsService
    {
        public List<FundExternalInvestmentDetailsModel> GetFundExternalInvestmentDetails()
        {
            List<FundExternalInvestmentDetailsModel> FundExternalInvestmentDetails = new List<FundExternalInvestmentDetailsModel>();
            string qryFundDetails = "SELECT wd.fn.code, { fn CONCAT(wd.fn.code, + ' - ' + wd.fn.short_name) } AS Fundnames, wd.wd_fn_header.fs_level_id" +
                                  " FROM wd.fn INNER JOIN" +
                                  " wd.wd_fn_header ON wd.fn.org_id = wd.wd_fn_header.org_id AND" +
                                  " wd.fn.session_id = wd.wd_fn_header.session_id AND" +
                                  " wd.fn.footnote_id = wd.wd_fn_header.footnote_id" +
                                  " WHERE (wd.wd_fn_header.fs_level_id = '2')";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                myConn.Open();
                using (SqlCommand objComm = new SqlCommand(qryFundDetails, myConn))
                {

                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FundExternalInvestmentDetails.Add(new FundExternalInvestmentDetailsModel { FundCode = reader["code"].ToString(), FundExternalInvestmentNames = reader["Fundnames"].ToString() });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return FundExternalInvestmentDetails;
                } //Command using end
            } //Conn using end
        } //Method end
    }//Class end
}//Namespace end