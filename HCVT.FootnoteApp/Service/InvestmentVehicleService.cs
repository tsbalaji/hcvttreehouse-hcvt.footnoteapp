﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;

namespace HCVT.FootnoteApp.Service
{
    public class InvestmentVehicleService
    {
        public List<InvestmentVehicleModel> GetInvestmentVehicleDetails(string org_id)
        {
            List<InvestmentVehicleModel> InvestmentVehicles = new List<InvestmentVehicleModel>();
            string qryFootnotes = "SELECT [company_id],CONCAT(code,' - ',description) as VehicleDesc FROM [wd].[company] WHERE org_id ='" + org_id + "'";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                myConn.Open();
                using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                {
                    
                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                InvestmentVehicles.Add(new InvestmentVehicleModel { CompanyID = reader["company_id"].ToString(), InvestmentVehicleName = reader["VehicleDesc"].ToString() });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return InvestmentVehicles;
                } //Command using end
            } //Conn using end
        } //Method end
    } // Class end
} //Namespace end