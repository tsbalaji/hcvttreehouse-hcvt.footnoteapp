﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;
namespace HCVT.FootnoteApp.Service
{
    public class TextDetailsService
    {
        public List<TextDetailsModel> GetTextDetails(string secID)
        {
            List<TextDetailsModel> TextDetails = new List<TextDetailsModel>();
            string qryFootnotes = " select sft.id,free_text,is_bold,is_italic,is_underline from wd.wd_fn_section_free_text_line sft" +
                                  " join wd.wd_fn_section s on s.id = sft.section_id" +
                                  " where sft.section_id = " + secID + " order by sft.sort_order";
            //s.footnote_id = 13 and - ToDo Need to add this condition
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                myConn.Open();
                using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                {
                    
                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TextDetails.Add(new TextDetailsModel {section_id = secID, free_text_id=reader["id"].ToString(),free_text = reader["free_text"].ToString(), IsBold = bool.Parse(reader["is_bold"].ToString()), IsItalics = bool.Parse(reader["is_italic"].ToString()), IsUnderline = bool.Parse(reader["is_underline"].ToString()) });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return TextDetails;
                } //Command using end
            } //Conn using end
        } //Method end
    } // Class end
} //Namespace end