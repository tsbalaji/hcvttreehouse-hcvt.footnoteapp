﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;

namespace HCVT.FootnoteApp.Service
{
    public class DealsService
    {
        public List<DealsModel> GetDealsDetails(string org_id)
        {
            List<DealsModel> Deals = new List<DealsModel>();
            string qryFootnotes = "SELECT [deal_id],[code],CONCAT(code,' - ',description) as DealDesc FROM [wd].[deal] where org_id ='" + org_id + "'";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                {
                    myConn.Open();
                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Deals.Add(new DealsModel { DealID=reader["deal_id"].ToString(), DealName = reader["DealDesc"].ToString() });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return Deals;
                } //Command using end
            } //Conn using end
        } //Method end
    } //Class end
} //Namespace end