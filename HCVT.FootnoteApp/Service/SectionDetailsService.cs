﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using HCVT.FootnoteApp.Models;

namespace HCVT.FootnoteApp.Service
{
    public class SectionDetailsService
    {
        public List<SectionDetailsModel> GetSectionDetails(string selectedFootnoteId)
        {
            List<SectionDetailsModel> SectionDetails = new List<SectionDetailsModel>();
            string qryFootnotes = "select s.id,section_type_id,st.short_name,s.sort_order from wd.wd_fn_section s" +
                                  " join wd.fn_section_type st on st.id = s.section_type_id" +
                                  " where footnote_id = '" + selectedFootnoteId + "' order by s.sort_order";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
            {
                myConn.Open();
                using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                {
                    
                    using (SqlDataReader reader = objComm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SectionDetails.Add(new SectionDetailsModel {sections_id = reader["id"].ToString(), section_type_id = reader["section_type_id"].ToString(), SectionName = reader["short_name"].ToString() });
                            } //reader while end
                        } //reader if end
                    } //reader using end
                    myConn.Close();
                    return SectionDetails;
                } //Command using end
            } //Conn using end
        } //Method end
    }// Class end
} // Namespace end