﻿using System.Collections.Generic;
using System.Data.SqlClient;
using HCVT.FootnoteApp.Models;
using System.Configuration;
using System;
namespace HCVT.FootnoteApp.Service
{
    public class FootnoteService
    {
        public List<FootnoteModel> GetFootnoteDetails(string org_id, string session_id, string code)
        {
            List<FootnoteModel> FootnoteDetails = new List<FootnoteModel>();
            //string qryFootnotes = "SELECT [footnote_id],CONCAT(wd.fn.code, + ' - ' + wd.fn.short_name) AS shortname FROM wd.fn"
            //                       + " WHERE (org_id = '" + org_id + "') AND (session_id = '" + session_id + "') AND (code = '" + code + "')";
            string qryFootnotes = "SELECT wd.fn.footnote_id,wd.wd_fn_header.deal_id,wd.wd_fn_header.direct_investment_vehicle_id,{ fn CONCAT(wd.fn.code, + ' - ' + wd.fn.short_name) } AS footnotename, wd.wd_fn_header.fs_level_id" +
                                    " FROM wd.fn INNER JOIN" +
                                    " wd.wd_fn_header ON wd.fn.org_id = wd.wd_fn_header.org_id AND" +
                                    " wd.fn.session_id = wd.wd_fn_header.session_id AND" +
                                    " wd.fn.footnote_id = wd.wd_fn_header.footnote_id" +
                                    " WHERE (wd.fn.org_id = '" + org_id + "') AND (wd.fn.session_id = '" + session_id + "') AND (wd.fn.code = '" + code + "')";
            using (SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FootnoteDBConnection"].ToString()))
                {
                    myConn.Open();
                    using (SqlCommand objComm = new SqlCommand(qryFootnotes, myConn))
                    {
                        
                        using (SqlDataReader reader = objComm.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    FootnoteDetails.Add(new FootnoteModel { footnoteid = reader["footnote_id"].ToString(), dealid = reader["deal_id"].ToString(), vehicleid = reader["direct_investment_vehicle_id"].ToString(), FootnoteName = reader["footnotename"].ToString() });
                                } //reader while end
                            } //reader if end
                        } //reader using end
                        myConn.Close();
                        return FootnoteDetails;
                    } //Command using end
                } //Conn using end
         } //Method end
    } //Class end
} //Namespace end